// Copyright (C) 2018,2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit;

import java.util.Random;
import java.util.function.Consumer;

import org.bukkit.Chunk;
import org.bukkit.HeightMap;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import de.ixilon.minecraft.MinecraftPlayer;
import net.openstreetcraft.api.model.MinecraftLocation;

/**
 * This abstract factory creates an abstract player of a concrete Bukkit player instance. 
 */
public class BukkitMinecraftPlayer implements MinecraftPlayer {
  
  private final JavaPlugin plugin;
  private final Player player;
  
  private BukkitMinecraftPlayer(JavaPlugin plugin, Player player) {
    this.plugin = plugin;
    this.player = player;
  }

  public static MinecraftPlayer of(JavaPlugin plugin, Player player) {
    return new BukkitMinecraftPlayer(plugin, player);
  }

  @Override
  public void sendMessage(String message) {
    player.sendMessage(message);
  }

  @Override
  public MinecraftLocation getMinecraftLocation() {
    Location location = player.getLocation();
    return new MinecraftLocation(location.getBlockX(), location.getBlockZ());
  }

  @Override
  public void setMinecraftLocation(MinecraftLocation minecraftLocation) {
    World world = plugin.getServer().createWorld(new WorldCreator("earth"));
    BukkitScheduler scheduler = plugin.getServer().getScheduler();
    scheduler.runTaskAsynchronously(plugin, () -> {
      Location location = player.getLocation();

      location.setWorld(world);
      location.setX(minecraftLocation.getX());
      location.setZ(minecraftLocation.getZ());
      location.setY(getBaseHeight(world, location.getBlockX(), location.getBlockZ()) + 1);

      Consumer<Chunk> onChunkLoaded = chunk -> player.teleportAsync(location);
      world.getChunkAtAsync(location, onChunkLoaded);
    });
  }

  private static int getBaseHeight(World world, int blockX, int blockZ) {
    ChunkGenerator generator = world.getGenerator();
    Random random = new Random();
    return generator.getBaseHeight(world, random, blockX, blockZ, HeightMap.WORLD_SURFACE);
  }

}