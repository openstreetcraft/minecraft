// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.terrain;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.BiomeProvider;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;
import org.springframework.web.client.RestTemplate;

import de.ixilon.minecraft.bukkit.BukkitBiomeConverter;
import de.ixilon.minecraft.plugin.terrain.Terrain;
import de.ixilon.minecraft.plugin.terrain.TerrainImpl;
import net.openstreetcraft.api.model.Cover;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.rest.RestTemplateFactory;

public class TerrainPlugin extends JavaPlugin {
  private static final BukkitBiomeConverter biomeConverter = new BukkitBiomeConverter();
  private Terrain terrain;

  @Override
  public void onEnable() {
    RestTemplateFactory factory = new RestTemplateFactory(getClassLoader());
    RestTemplate restTemplate = factory.getRestTemplate();
    int distance = Bukkit.getViewDistance() * 2;
    int maxChunks = Bukkit.getMaxPlayers() * distance * distance;
    terrain = new TerrainImpl(restTemplate, maxChunks);
  }

  @Override
  public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
    getLogger().info("install chunk generator for world \"" + worldName + "\"");
    return new CustomChunkGenerator(terrain);
  }

  @Override
  public BiomeProvider getDefaultBiomeProvider(String worldName, String id) {
    getLogger().info("install biome provider for world \"" + worldName + "\"");
    return new CustomBiomeProvider(terrain);
  }

  public int surface(World world, Location location) {
    return terrain.surface(cast(world), cast(location));
  }

  public Biome biome(World world, Location location) {
    return biomeConverter.apply(terrain.biome(cast(world), cast(location)));
  }

  public Cover cover(World world, Location location) {
    return terrain.cover(cast(world), cast(location));
  }

  public SphericalLocation sphericalLocation(World world, Location location) {
    return terrain.sphericalLocation(cast(world), cast(location));
  }

  public float plastic(World world, Location location) {
    return terrain.plastic(cast(world), cast(location));
  }

  private static net.openstreetcraft.api.model.World cast(org.bukkit.World world)
      throws IllegalArgumentException {
    return net.openstreetcraft.api.model.World.of(world.getName());
  }

  private static MinecraftLocation cast(Location location) {
    return new MinecraftLocation(location.getBlockX(), location.getBlockZ());
  }

}
