// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.terrain;

import java.util.Random;

import org.bukkit.HeightMap;
import org.bukkit.Material;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.generator.WorldInfo;

import de.ixilon.minecraft.plugin.biome.Biomes;
import de.ixilon.minecraft.plugin.terrain.Terrain;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.minecraft.biome.Biome;

/**
 * Custom chunk generator using the new Bukkit chunk generation API. Generation steps:
 * <ul>
 * <li>Bedrock (default)
 * <li>Caves (disabled)
 * <li>Decorations (default)
 * <li>Mobs (default)
 * <li>Noise (custom)
 * <li>Structures (disabled)
 * <li>Surface (default)
 * </ul>
 */
public class CustomChunkGenerator extends ChunkGenerator {

  private static final World WORLD = World.EARTH;
  private static final int SEALEVEL = 63;
  private static final int CHUNK_SIZE = 16;

  private final Terrain terrain;

  public CustomChunkGenerator(Terrain terrain) {
    this.terrain = terrain;
  }

  /**
   * Generate terrain using OpenStreetCraft API.
   */
  @Override
  public void generateNoise(WorldInfo world, Random random, int chunkX, int chunkZ,
      ChunkData chunk) {
    for (int blockX = chunkX << 4, dx = 0; dx < CHUNK_SIZE; blockX++, dx++) {
      for (int blockZ = chunkZ << 4, dz = 0; dz < CHUNK_SIZE; blockZ++, dz++) {
        int floor = getBaseHeight(world, random, blockX, blockZ, HeightMap.OCEAN_FLOOR);
        int surface = getBaseHeight(world, random, blockX, blockZ, HeightMap.WORLD_SURFACE);
        synchronized (terrain) {
          chunk.setRegion(dx, chunk.getMinHeight(), dz, dx + 1, floor + 1, dz + 1, Material.STONE);
          chunk.setRegion(dx, floor, dz, dx + 1, surface, dz + 1, Material.WATER);
        }
      }
    }
  }

  /**
   * This method was added to also bring the Minecraft generator and Bukkit generator more in line.
   * With this it is possible to return the max height of a location (before decorations). This is
   * useful to let most structures know were to place them.
   */
  @Override
  public int getBaseHeight(WorldInfo world, Random random, int blockX, int blockZ, HeightMap type) {
    MinecraftLocation location = new MinecraftLocation(blockX, blockZ);
    int height = terrain.surface(WORLD, location);

    height += SEALEVEL;
    height = Math.min(height, world.getMaxHeight() - 24);
    height = Math.max(height, world.getMinHeight() + 5);

    Biome biome = terrain.biome(WORLD, location);

    if (Biomes.isOcean(biome)) {
      switch (type) {
        case WORLD_SURFACE:
        case WORLD_SURFACE_WG:
          return SEALEVEL;
        default:
          height = Math.min(height, SEALEVEL);
      }
    }

    switch (type) {
      case OCEAN_FLOOR:
      case OCEAN_FLOOR_WG:
        if (height >= SEALEVEL && Biomes.isWater(biome)) {
          return height - 1;
        }
        break;
      default:
    }

    return height;
  }

  @Override
  public boolean shouldGenerateBedrock() {
    return true;
  }

  @Override
  public boolean shouldGenerateCaves() {
    return false;
  }

  @Override
  public boolean shouldGenerateDecorations() {
    return true;
  }

  @Override
  public boolean shouldGenerateMobs() {
    return true;
  }

  @Override
  public boolean shouldGenerateNoise() {
    return false; // We do this ourselves
  }

  @Override
  public boolean shouldGenerateStructures() {
    return false;
  }

  @Override
  public boolean shouldGenerateSurface() {
    return true;
  }
  
}
