// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.terrain;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.block.Biome;
import org.bukkit.generator.BiomeProvider;
import org.bukkit.generator.WorldInfo;

import de.ixilon.minecraft.bukkit.BukkitBiomeConverter;
import de.ixilon.minecraft.plugin.terrain.Terrain;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.World;

/**
 * The BiomeProvider acts as Biome source and wrapper for the NMS class WorldChunkManager. With this
 * the underlying Vanilla ChunkGeneration knows which Biome to use for the structure and decoration
 * generation. (Fixes: SPIGOT-5880). Although the List of Biomes which is required in BiomeProvider,
 * is currently not much in use in Vanilla, I decided to add it to future proof the API when it may
 * be required in later versions of Minecraft. The BiomeProvider is also separated from the
 * ChunkGenerator for plugins which only want to change the biome map, such as single Biome worlds
 * or if some biomes should be more present than others.
 */
public class CustomBiomeProvider extends BiomeProvider {

  private static final BukkitBiomeConverter BIOME_CONVERTER = new BukkitBiomeConverter();
  private final Terrain terrain;
  
  public CustomBiomeProvider(Terrain terrain) {
    this.terrain = terrain;
  }

  @Override
  public Biome getBiome(WorldInfo worldInfo, int blockX, int blockY, int blockZ) {
    MinecraftLocation location = new MinecraftLocation(blockX, blockZ);
    return BIOME_CONVERTER.apply(terrain.biome(World.EARTH, location));
  }

  @Override
  public List<Biome> getBiomes(WorldInfo worldInfo) {
    return Stream.of(net.openstreetcraft.minecraft.biome.Biome.values())
        .map(BIOME_CONVERTER::apply)
        .collect(Collectors.toList());
  }

}
