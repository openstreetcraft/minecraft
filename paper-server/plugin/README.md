# Paper plugins

This directory contains all the code which is needed to build and deploy plugins for the Paper 1.17 Minecraft server.
Unlike the other server types (which are build with Gradle 4), the build system of this directory is based on Gradle 7.3 which requires at least Java 17.

The `build.gradle` of this directory is not part of the `settings.gradle` in the root project.
You have to change into this directory if you want to build the Paper 1.17 plugins:

## Plugins for Paper 1.17

```
export JAVA_HOME=<path to JDK 17>
cd paper-server/plugin
./gradlew :terrain:runtime:runServer  
```

See also: [Plugins for Paper 1.16](../README.md)

## Eclipse setup

Eclipse project files are created with

```
./gradlew eclipse
```

The generated projects are using the Default JRE of the Eclipse workspace.
The installed Eclipse IDE must support at least Java 8 and a suitable JRE must be configured in the workspace settings.

If you want to use the Eclipse Gradle plugin, then you have to install a Eclipse version which supports Java 17.
With older Eclipse versions, Gradle must be run on the commandline.

## Deployment

Plugin jar files are deployed to the [GitLab Package Registry](https://gitlab.com/openstreetcraft/minecraft/-/packages) with:

```
./gradlew publish
```

on the commandline (requires [credentials](https://docs.gitlab.com/ee/user/packages/maven_repository/#authenticate-to-the-package-registry-with-gradle)) or [automatically with GitLab CI](https://docs.gitlab.com/ee/user/packages/maven_repository/#create-maven-packages-with-gitlab-cicd-by-using-gradle).
