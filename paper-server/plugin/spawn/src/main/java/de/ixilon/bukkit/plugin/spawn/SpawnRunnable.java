// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.spawn;

import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import org.springframework.web.client.RestTemplate;

import de.ixilon.bukkit.plugin.terrain.TerrainPlugin;
import de.ixilon.minecraft.plugin.biome.Biomes;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.minecraft.biome.Biome;
import net.openstreetcraft.minecraft.biome.BiomeParser;
import net.openstreetcraft.minecraft.biome.JavaBiome;

public class SpawnRunnable extends BukkitRunnable {

  private static final BiomeParser<JavaBiome> BIOME_PARSER = new BiomeParser<>(JavaBiome.class);

  private final JavaPlugin plugin;
  private final World world;
  private final CoordinatesService coordinatesService;
  private final Set<Biome> biomes;
  
  private boolean canceled = false;

  /**
   * Calculate a player spawn that is outside of water.
   */
  public SpawnRunnable(JavaPlugin plugin, ClassLoader classLoader, World world) {
    this.plugin = plugin;
    this.world = world;
    this.coordinatesService = new CoordinatesService(createRestTemplate(classLoader));
    this.biomes = plugin.getConfig().getStringList("biomes").stream()
        .map(String::toUpperCase)
        .map(Biome::valueOf)
        .collect(Collectors.toSet());
  }

  private static RestTemplate createRestTemplate(ClassLoader classLoader) {
    RestTemplateFactory factory = new RestTemplateFactory(classLoader);
    return factory.getRestTemplate();
  }
  
  @Override
  public void cancel() {
    canceled = true;
  }

  private Logger logger() {
    return plugin.getLogger();
  }

  @Override
  public void run() {
    PluginManager pluginManager = plugin.getServer().getPluginManager();
    String pluginName = "openstreetcraft-terrain";
    TerrainPlugin terrain = (TerrainPlugin) pluginManager.getPlugin(pluginName);
    if (terrain == null) {
      logger().log(Level.WARNING, "plugin is not loaded: " + pluginName);
      return;
    }
    for (SphericalLocation sphericalLocation : randomSphericalLocations()) {
      if (canceled) {
        return;
      }
      Location location = toLocation(sphericalLocation);
      final Biome biome;
      try {
        biome = BIOME_PARSER.parse(terrain.biome(world, location).ordinal());
      } catch (IllegalArgumentException exception) {
        logger().log(Level.FINE, exception.getMessage());
        continue;
      }
      if (isValid(biome)) {
        Consumer<Chunk> onChunkLoaded = chunk -> {
          if (canceled) {
            return;
          }
          int blockY = world.getHighestBlockYAt(location);
          location.setY(blockY + 1);
          BukkitScheduler scheduler = plugin.getServer().getScheduler();
          scheduler.runTask(plugin, () -> {
            world.setSpawnLocation(location);
            String message = String.format("set %s spawn to %s",
                world.getName(), location.toString());
            logger().log(Level.INFO, message);
          });
        };
        if (!canceled) {
          world.getChunkAtAsync(location, onChunkLoaded);
        }
        return;
      }
    }
    throw new IllegalStateException();
  }

  private boolean isValid(Biome biome) {
    if (!biomes.isEmpty()) {
      return biomes.contains(biome);
    }
    return !Biomes.isWater(biome);
  }

  private Location toLocation(SphericalLocation sphericalLocation) {
    MinecraftLocation minecraftLocation = coordinatesService.getMinecraftLocation(sphericalLocation,
        net.openstreetcraft.api.model.World.of(world.getName()));
    return new Location(world, minecraftLocation.getX(), 0, minecraftLocation.getZ());
  }

  private static Iterable<SphericalLocation> randomSphericalLocations() {
    return new Iterable<SphericalLocation>() {
      @Override
      public Iterator<SphericalLocation> iterator() {
        return new Iterator<SphericalLocation>() {
          private final Random random = new Random();

          @Override
          public boolean hasNext() {
            return true;
          }

          @Override
          public SphericalLocation next() {
            return new SphericalLocation(randomDouble(90.0), randomDouble(180.0));
          }

          private double randomDouble(double bound) {
            double number = bound * random.nextDouble();
            return random.nextBoolean() ? -number : number;
          }
        };
      }
    };
  }

}
