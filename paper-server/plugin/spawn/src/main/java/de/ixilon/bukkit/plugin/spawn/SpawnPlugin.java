// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.spawn;

import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

public class SpawnPlugin extends JavaPlugin {

  private SpawnListener listener;
  
  @Override
  public void onEnable() {
    saveDefaultConfig();
    Server server = getServer();
    if (server != null) {
      listener = new SpawnListener(this, getClassLoader());
      server.getPluginManager().registerEvents(listener, this);
      World world = server.getWorld("earth");
      if (world != null) {
        listener.computeWorldSpawn(world);
      }
    }
  }

  @Override
  public void onDisable() {
    if (listener != null) {
      listener.cancel();
      listener = null;
    }
  }
}
