// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.spawn;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

public class SpawnListener implements Listener {

  private final JavaPlugin plugin;
  private final ClassLoader classLoader;

  private SpawnRunnable runnable;
  private BukkitTask task;

  public SpawnListener(JavaPlugin plugin, ClassLoader classLoader) {
    this.plugin = plugin;
    this.classLoader = classLoader;
  }

  /**
   * A new world spawn is computed after a new player joins the server.
   */
  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();
    World world = player.getWorld();

    if (!world.getName().equals("earth")) {
      return;
    }

    if (player.hasPlayedBefore()) {
      return;
    }

    player.setBedSpawnLocation(world.getSpawnLocation(), true);
    computeWorldSpawn(world);
  }

  void computeWorldSpawn(World world) {
    if (task != null && getScheduler().isCurrentlyRunning(task.getTaskId())) {
      return;
    }

    logger().log(Level.INFO, String.format("computing %s spawn ...", world.getName()));

    runnable = new SpawnRunnable(plugin, classLoader, world);
    task = runnable.runTaskAsynchronously(plugin);
  }

  public void cancel() {
    runnable.cancel();
  }

  private BukkitScheduler getScheduler() {
    return plugin.getServer().getScheduler();
  }
  
  private Logger logger() {
    return plugin.getLogger();
  }

}
