# Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

stages:
- test
- build
- push
- release
- deploy

variables:
  GRADLE_IMAGE: openjdk:17

check:
  stage: test
  image: $GRADLE_IMAGE
  script: ./gradlew check
  artifacts:
    when: on_failure
    expire_in: 1 week
    paths:
      - build/reports

build-bukkit-server:
  stage: build
  image: $GRADLE_IMAGE
  script: ./gradlew :bukkit-server:prepareDocker
  artifacts:
    expire_in: 1d
    paths:
      - bukkit-server/build/docker

build-paper-server:
  stage: build
  image: $GRADLE_IMAGE
  script: ./gradlew :paper-server:prepareDocker
  artifacts:
    expire_in: 1d
    paths:
      - paper-server/build/docker

build-nukkit-server:
  stage: build
  image: $GRADLE_IMAGE
  script: ./gradlew installLatestNukkitServer
  artifacts:
    expire_in: 1d
    paths:
      - nukkit-server/build/libs
      - nukkit-server/build/public

.docker_template: &docker_defs
  image: docker:git
  services:
    - docker:dind

push-bukkit-server:
  stage: push
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/bukkit-server:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $BRANCH_REGISTRY bukkit-server/build/docker
    - docker push $BRANCH_REGISTRY

release-bukkit-server:
  stage: release
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/bukkit-server:$CI_COMMIT_REF_NAME
    COMMIT_REGISTRY: $CI_REGISTRY_IMAGE/bukkit-server:$CI_COMMIT_SHA
    LATEST_REGISTRY: $CI_REGISTRY_IMAGE/bukkit-server:latest
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $BRANCH_REGISTRY
    - docker tag  $BRANCH_REGISTRY $COMMIT_REGISTRY
    - docker tag  $BRANCH_REGISTRY $LATEST_REGISTRY
    - docker push $COMMIT_REGISTRY
    - docker push $LATEST_REGISTRY
  only:
    - master

push-paper-server:
  stage: push
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/paper-server:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $BRANCH_REGISTRY paper-server/build/docker
    - docker push $BRANCH_REGISTRY

release-paper-server:
  stage: release
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/paper-server:$CI_COMMIT_REF_NAME
    COMMIT_REGISTRY: $CI_REGISTRY_IMAGE/paper-server:$CI_COMMIT_SHA
    LATEST_REGISTRY: $CI_REGISTRY_IMAGE/paper-server:latest
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $BRANCH_REGISTRY
    - docker tag  $BRANCH_REGISTRY $COMMIT_REGISTRY
    - docker tag  $BRANCH_REGISTRY $LATEST_REGISTRY
    - docker push $COMMIT_REGISTRY
    - docker push $LATEST_REGISTRY
  only:
    - master

push-proxy-server:
  stage: push
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/proxy-server:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $BRANCH_REGISTRY proxy-server
    - docker push $BRANCH_REGISTRY

release-proxy-server:
  stage: release
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/proxy-server:$CI_COMMIT_REF_NAME
    COMMIT_REGISTRY: $CI_REGISTRY_IMAGE/proxy-server:$CI_COMMIT_SHA
    LATEST_REGISTRY: $CI_REGISTRY_IMAGE/proxy-server:latest
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $BRANCH_REGISTRY
    - docker tag  $BRANCH_REGISTRY $COMMIT_REGISTRY
    - docker tag  $BRANCH_REGISTRY $LATEST_REGISTRY
    - docker push $COMMIT_REGISTRY
    - docker push $LATEST_REGISTRY
  only:
    - master

push-nukkit-server:
  stage: push
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/nukkit-server:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $BRANCH_REGISTRY nukkit-server
    - docker push $BRANCH_REGISTRY
  when: manual

release-nukkit-server:
  stage: release
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/nukkit-server:$CI_COMMIT_REF_NAME
    COMMIT_REGISTRY: $CI_REGISTRY_IMAGE/nukkit-server:$CI_COMMIT_SHA
    LATEST_REGISTRY: $CI_REGISTRY_IMAGE/nukkit-server:latest
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $BRANCH_REGISTRY
    - docker tag  $BRANCH_REGISTRY $COMMIT_REGISTRY
    - docker tag  $BRANCH_REGISTRY $LATEST_REGISTRY
    - docker push $COMMIT_REGISTRY
    - docker push $LATEST_REGISTRY
  only:
    - master
  when: manual
    
push-forge-server:
  stage: push
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/forge-server:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $BRANCH_REGISTRY forge-server
    - docker push $BRANCH_REGISTRY
  when: manual

release-forge-server:
  stage: release
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/forge-server:$CI_COMMIT_REF_NAME
    COMMIT_REGISTRY: $CI_REGISTRY_IMAGE/forge-server:$CI_COMMIT_SHA
    LATEST_REGISTRY: $CI_REGISTRY_IMAGE/forge-server:latest
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $BRANCH_REGISTRY
    - docker tag  $BRANCH_REGISTRY $COMMIT_REGISTRY
    - docker tag  $BRANCH_REGISTRY $LATEST_REGISTRY
    - docker push $COMMIT_REGISTRY
    - docker push $LATEST_REGISTRY
  only:
    - master
  when: manual
    
push-spigot-server:
  stage: push
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/spigot-server:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $BRANCH_REGISTRY spigot-server
    - docker push $BRANCH_REGISTRY
  when: manual

release-spigot-server:
  stage: release
  <<: *docker_defs
  variables:
    BRANCH_REGISTRY: $CI_REGISTRY_IMAGE/spigot-server:$CI_COMMIT_REF_NAME
    COMMIT_REGISTRY: $CI_REGISTRY_IMAGE/spigot-server:$CI_COMMIT_SHA
    LATEST_REGISTRY: $CI_REGISTRY_IMAGE/spigot-server:latest
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $BRANCH_REGISTRY
    - docker tag  $BRANCH_REGISTRY $COMMIT_REGISTRY
    - docker tag  $BRANCH_REGISTRY $LATEST_REGISTRY
    - docker push $COMMIT_REGISTRY
    - docker push $LATEST_REGISTRY
  only:
    - master
  when: manual

bukkit-creeper:
  stage: deploy
  image: docker/compose:1.26.2
  environment: hosting
  variables:
    DOCKER_HOST: ssh://ansible@creeper.openstreetcraft.net
    COMPOSE_PROJECT_NAME: minecraft
    OPENSTREETCRAFT_HOST: creeper
  before_script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker-compose --file=bukkit-server/docker-compose.prod.yml up -d --remove-orphans
  only:
    - master
  when: manual

paper-creeper:
  stage: deploy
  image: docker/compose:1.26.2
  environment: hosting
  variables:
    DOCKER_HOST: ssh://ansible@creeper.openstreetcraft.net
    COMPOSE_PROJECT_NAME: minecraft
    OPENSTREETCRAFT_HOST: creeper
  before_script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker-compose --file=paper-server/docker-compose.prod.yml up -d --remove-orphans
  only:
    - master

bukkit-eu-central-1:
  stage: deploy
  image: sftech/ecs-cli
  environment: prod
  variables:
    AWS_REGION: eu-central-1
  script:
    - ecs-cli configure --cluster prod
    - cd bukkit-server && ecs-cli compose -f docker-compose.yml -f docker-compose.prod.yml service up
  only:
    - master
  when: manual

nukkit-eu-central-1:
  stage: deploy
  image: sftech/ecs-cli
  environment: prod
  variables:
    AWS_REGION: eu-central-1
  script:
    - ecs-cli configure --cluster prod
    - cd nukkit-server && ecs-cli compose -f docker-compose.yml -f docker-compose.prod.yml service up
  only:
    - master
  when: manual
  
forge-eu-central-1:
  stage: deploy
  image: sftech/ecs-cli
  environment: prod
  variables:
    AWS_REGION: eu-central-1
  script:
    - ecs-cli configure --cluster prod
    - cd forge-server && ecs-cli compose -f docker-compose.yml -f docker-compose.prod.yml service up
  only:
    - master
  when: manual
  
spigot-eu-central-1:
  stage: deploy
  image: sftech/ecs-cli
  environment: prod
  variables:
    AWS_REGION: eu-central-1
  script:
    - ecs-cli configure --cluster prod
    - cd spigot-server && ecs-cli compose -f docker-compose.yml -f docker-compose.prod.yml service up
  only:
    - master
  when: manual

pages:
  stage: deploy
  environment:
    name: download
    url: https://openstreetcraft.gitlab.io/minecraft/download/nukkit/nukkit-server-latest.zip
  script:
  - mv nukkit-server/build/public public
  artifacts:
    paths:
    - public
  only:
    - master
