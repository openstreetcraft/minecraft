# Prepare for Docker Compose

```
./gradlew buildDocker
export HOSTIP=<your ip address here>
```

# Bukkit server

To start a Bukkit server with all plugins enabled:

```
docker-compose up java
```

# Proxy server

To start a proxy server on TCP port 25577 (multi-version support) and on UDP port 19132 (Bedrock server)
in addition to the Bukkit server on TCP port 25565:

```
docker-compose up proxy
```
