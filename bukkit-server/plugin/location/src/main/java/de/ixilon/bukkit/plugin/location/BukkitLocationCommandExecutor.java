// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.location;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.ixilon.minecraft.bukkit.BukkitMinecraftPlayer;
import de.ixilon.minecraft.plugin.location.LocationCommandExecutor;

public class BukkitLocationCommandExecutor implements CommandExecutor {

  private final JavaPlugin plugin;
  private final LocationCommandExecutor executor;

  public BukkitLocationCommandExecutor(JavaPlugin plugin, ClassLoader classLoader) {
    this.plugin = plugin;
    executor = new LocationCommandExecutor(classLoader);
  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender instanceof Player) {
      executor.onCommand(BukkitMinecraftPlayer.of(plugin, (Player) sender), args);
      return true;
    }
    return false;
  }
}
