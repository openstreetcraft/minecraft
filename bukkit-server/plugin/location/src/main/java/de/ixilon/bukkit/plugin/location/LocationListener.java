// Copyright (C) 2018-2021 Gerald Fiedler <gerald@ixilon.de>, Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.location;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class LocationListener implements Listener {

  private final JavaPlugin plugin;
  
  /**
   * Constructor of LocationListener.
   */
  public LocationListener(JavaPlugin plugin) {
    this.plugin = plugin;
  }

  /**
   * Called when player joins the server.
   */
  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();
    performCommand(player);
  }

  /**
   * Called when player respawns.
   */
  @EventHandler
  public void onPlayerRespawn(PlayerRespawnEvent event) {
    performCommand(event.getPlayer());
  }

  /**
   * Called when player teleports to a new location.
   */
  @EventHandler
  public void onPlayerTeleport(PlayerTeleportEvent event) {
    if (event.getCause() == TeleportCause.PLUGIN) {
      performCommand(event.getPlayer());
    }
  }

  private void performCommand(final Player player) {
    getScheduler().scheduleSyncDelayedTask(plugin, () -> player.performCommand("location"));
  }

  private BukkitScheduler getScheduler() {
    return plugin.getServer().getScheduler();
  }

}
