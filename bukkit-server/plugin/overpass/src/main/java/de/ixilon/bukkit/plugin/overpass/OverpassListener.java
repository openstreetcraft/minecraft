// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>, Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.overpass;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkPopulateEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.ImmutableList;

import de.ixilon.minecraft.world.BukkitChunkCoordinate;
import de.ixilon.minecraft.world.ChunkCoordinate;
import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmTag;
import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.api.geom.Point;
import net.openstreetcraft.api.geom.RectangularBoundingBox;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.CachingCoordinatesService;
import net.openstreetcraft.api.service.CachingOverpassService;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.api.service.OverpassService;
import net.openstreetcraft.osm.geom.OsmNodeList;
import net.openstreetcraft.osm.geom.OsmPoint;
import net.openstreetcraft.osm.geom.Rectangle;
import net.openstreetcraft.osm.util.OsmUtils;
import net.openstreetcraft.osm.util.OsmWayUtils;

public class OverpassListener implements Listener {
  private final Plugin plugin;
  private final CoordinatesService coordinatesService;
  private final OverpassService overpassService;

  /**
   * Place signs with informations about OSM ways and nodes. 
   */
  public OverpassListener(OverpassPlugin plugin, ClassLoader classLoader) {
    RestTemplateFactory factory = new RestTemplateFactory(classLoader);
    RestTemplate restTemplate = factory.getRestTemplate();
    int cacheSize = Bukkit.getMaxPlayers() * Bukkit.getViewDistance();
    coordinatesService = new CachingCoordinatesService(restTemplate, cacheSize);
    overpassService = new CachingOverpassService(restTemplate, cacheSize);
    this.plugin = plugin;
  }

  /**
   * Called when a new chunk has finished being populated.
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onChunkPopulateEvent(ChunkPopulateEvent event) {
    World world = event.getWorld();
    Chunk chunk = event.getChunk();
    scheduler().runTaskAsynchronously(plugin, () -> onChunkPopulateEvent(world, chunk));
  }

  private void onChunkPopulateEvent(World world, Chunk chunk) {
    ChunkCoordinate chunkCoordinate = new BukkitChunkCoordinate(chunk);
    RectangularBoundingBox bbox = getBoundingBox(chunkCoordinate);
    Rectangle rect = toRectangle(bbox);
    Osm osm = overpassService.getOsm(bbox);
    Iterable<OsmNode> nodes = OsmUtils.getNodes(osm);
    OsmNodeList nodeList = OsmNodeList.copyOf(nodes);
    
    for (OsmWay way : OsmUtils.getWays(osm)) {
      Iterable<OsmTag> tags = OsmWayUtils.getTags(way);
      Material material = OsmWayUtils.isClosed(way) ? Material.OAK_SIGN : Material.SPRUCE_SIGN;
      for (OsmNode node : OsmWayUtils.getNodes(way, nodeList)) {
        if (isInside(rect, node)) {
          drawNode(world, node, tags, material);
        }
      }
    }
    
    for (OsmNode node : nodes) {
      if (isInside(rect, node)) {
        drawNode(world, node, node.getTag(), Material.BIRCH_SIGN);
      }
    }
  }

  private void drawNode(World world, OsmNode node, Iterable<OsmTag> tags, Material material) {
    SphericalLocation sphericalLocation = new SphericalLocation(node.getLat(), node.getLon());
    MinecraftLocation minecraftLocation = toMinecraftLocation(sphericalLocation);
    scheduler().runTask(plugin, () -> drawTags(world, minecraftLocation, tags, material));
  }

  private static void drawTags(World world, MinecraftLocation location, Iterable<OsmTag> tags,
      Material material) {
    int xblock = toInt(location.getX());
    int zblock = toInt(location.getZ());
    int xchunk = xblock >> 4;
    int zchunk = zblock >> 4;
    if (world.isChunkLoaded(xchunk, zchunk)) {
      Block block = world.getHighestBlockAt(xblock, zblock);
      for (OsmTag tag : tags) {
        block = block.getRelative(BlockFace.UP);
        block.setType(material);
        Sign sign = (Sign) block.getState();
        sign.setLine(0, tag.getK());
        sign.setLine(1, tag.getV());
        sign.update();
      }
    }
  }

  private static int toInt(long value) {
    return Math.toIntExact(value);
  }

  private static Rectangle toRectangle(RectangularBoundingBox bbox) {
    return Rectangle.builder()
        .xmin(bbox.getUpperLeft().getX())
        .ymin(bbox.getUpperLeft().getY())
        .xmax(bbox.getLowerRight().getX())
        .ymax(bbox.getLowerRight().getY())
        .build();
  }

  private static boolean isInside(Rectangle bbox, OsmNode node) {
    OsmPoint point = new OsmPoint(node);
    return point.isInside(bbox);
  }

  private RectangularBoundingBox getBoundingBox(ChunkCoordinate chunkCoordinate) {
    List<SphericalLocation> sphericalLocations = toSphericalLocations(ImmutableList.of(
        chunkCoordinate.nw().toMinecraftLocation(),
        chunkCoordinate.se().toMinecraftLocation()));
    
    return new RectangularBoundingBox.Builder()
        .withLowerLeft(toPoint(sphericalLocations.get(0)))
        .withUpperRight(toPoint(sphericalLocations.get(1)))
        .build();
  }

  private static Point toPoint(SphericalLocation location) {
    return new Point(location.getLongitude(), location.getLatitude());
  }
  
  private List<SphericalLocation> toSphericalLocations(List<MinecraftLocation> locations) {
    return coordinatesService.getSphericalLocations(locations, 
        net.openstreetcraft.api.model.World.EARTH);
  }

  private MinecraftLocation toMinecraftLocation(SphericalLocation location) {
    return coordinatesService.getMinecraftLocation(location,
        net.openstreetcraft.api.model.World.EARTH);
  }

  private BukkitScheduler scheduler() {
    return plugin.getServer().getScheduler();
  }
}
