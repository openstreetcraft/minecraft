# Bukkit Overpass Plugin

This plugin is a debug tool for developers to visualize Openstreetmap (OSM) structures within the Minecraft world.

For every OSM node a sign is placed on the map and labeled with the key/value pair of the node.

OSM nodes, closed and opened ways can be distinguished by the material of the sign (birch, oak or spruce).

## Installation

```
./gradlew jar
cp build/libs/openstreetcraft-overpass-0.1.0.jar ${plugin_base_dir}/build/server/1.15.2/plugins/
```
