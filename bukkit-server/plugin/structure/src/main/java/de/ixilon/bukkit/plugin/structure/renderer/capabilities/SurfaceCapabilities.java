// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.capabilities;

import de.ixilon.bukkit.plugin.structure.block.Material;
import de.ixilon.bukkit.plugin.structure.renderer.feature.Surface;

public class SurfaceCapabilities {

  private final Material material;

  /**
   * Rendering capabilites of surfaces. 
   */
  public SurfaceCapabilities(Surface surface) {
    switch (surface) {
      case ARTIFICAL_TURF:
      case GRASS:
      case GRASS_PAVER:
        material = Material.GRASS_PATH;
        break;
      case ASPHALT:
      case PAVED:
        material = Material.ANDESITE;
        break;
      case CLAY:
        material = Material.CLAY;
        break;
      case COBBLESTONE:
      case COBBLESTONE_FLATTENED:
        material = Material.COBBLESTONE;
        break;
      case COMPACTED:
        material = Material.GRAVEL;
        break;
      case CONCRETE:
      case CONCRETE_LANES:
      case CONCRETE_PLATES:
        material = Material.POLISHED_DIORITE;
        break;
      case DECOTURF:
      case TARTAN:
        material = Material.RED_CARPET;
        break;
      case DIRT:
      case EARTH:
      case GROUND:
      case MUD:
        material = Material.DIRT;
        break;
      case FINE_GRAVEL:
      case GRAVEL:
      case GRAVEL_TURF:
      case PEBBLESTONE:
      case SETT:
        material = Material.GRAVEL;
        break;
      case ICE:
        material = Material.ICE;
        break;
      case METAL:
      case METALL_GRID:
        material = Material.IRON_BLOCK;
        break;
      case PAVING_STONES:
        material = Material.GRANITE;
        break;
      case SALT:
        material = Material.GLASS;
        break;
      case SAND:
        material = Material.SAND;
        break;
      case SNOW:
        material = Material.SNOW;
        break;
      case UNPAVED:
        material = Material.GRAVEL;
        break;
      case WOOD:
      case WOODCHIPS:
        material = Material.DARK_OAK_WOOD;
        break;
      default:
        throw new IllegalArgumentException(surface.toString());
    }
  }

  public final Material getMaterial() {
    return material;
  }

}
