// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.block;

public class BukkitMaterial {
  /**
   * Convert generic material into Bukkit material.
   */
  public static org.bukkit.Material of(Material material) throws UnknownMaterialException {
    String name = material.name();
    try {
      return Enum.valueOf(org.bukkit.Material.class, material.name());
    } catch (IllegalArgumentException exception) {
      throw new UnknownMaterialException(name);
    }
  }

  /**
   * Convert Bukkit material into generic material.
   */
  public static Material parse(org.bukkit.Material material) throws UnknownMaterialException {
    String name = material.getKey().getKey().toUpperCase();
    try {
      return Enum.valueOf(Material.class, name);
    } catch (IllegalArgumentException exception) {
      throw new UnknownMaterialException(name);
    }
  }
}
