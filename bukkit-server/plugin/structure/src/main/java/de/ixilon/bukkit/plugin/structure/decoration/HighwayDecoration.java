// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.decoration;

import java.util.Random;

import de.ixilon.bukkit.plugin.structure.renderer.BukkitRenderImage;
import de.ixilon.bukkit.plugin.structure.renderer.HighwayRenderer;
import de.ixilon.bukkit.plugin.structure.renderer.RenderImage;
import de.ixilon.bukkit.plugin.structure.renderer.RenderModel;
import de.ixilon.bukkit.plugin.structure.renderer.Renderer;

public class HighwayDecoration implements Decoration {

  private final DecorationAreaRepository repository;

  public HighwayDecoration(DecorationAreaRepository repository) {
    this.repository = repository;
  }

  @Override
  public void decorate(DecorationArea area, Random random) {
    RenderImage image = new BukkitRenderImage(area);
    RenderModel model = repository.getRenderModel(area);
    Renderer renderer = new HighwayRenderer();
    renderer.render(image, model);
  }

}
