// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import com.google.common.base.Optional;

import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.osm.geom.Circle2D;
import net.openstreetcraft.osm.geom.OsmPolyline;
import net.openstreetcraft.osm.geom.Rectangle;

public abstract class PolylineRenderer extends WayRenderer {

  private OsmPolyline polyline;
  private Optional<Double> width = Optional.absent();

  @Override
  protected boolean isInside(Rectangle bbox) {
    if (width.isPresent()) {
      return getPolyline().isInside(new Circle2D(bbox, width.get() / 2));
    } else {
      return getPolyline().isInside(bbox);
    }
  }

  @Override
  protected void setWay(OsmWay way) {
    this.polyline = null;
    this.width = Optional.absent();
    super.setWay(way);
  }

  protected final OsmPolyline getPolyline() {
    if (polyline == null) {
      polyline = new OsmPolyline(getWay(), getNodes());
    }
    return polyline;
  }

  protected final void setWidth(RenderModel model, Optional<String> meters) {
    if (meters.isPresent()) {
      setWidth(model, meters.get());
    }
  }
  
  protected final void setWidth(RenderModel model, String meters) {
    try {
      setWidth(model, Double.parseDouble(meters));
    } catch (NumberFormatException exception) {
      // use default
    }
  }

  protected final void setWidth(RenderModel model, double meters) {
    this.width = Optional.of(model.toDegree(meters));
  }

  protected final double getDistanceInMeter(RenderModel model, Rectangle bbox) {
    return model.toMeter(getPolyline().distance(bbox)); 
  }
  
}
