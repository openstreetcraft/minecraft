// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.feature;

/**
 * Fahrradwege.
 */
public enum Cycleway {
  /**
   * Radverkehrsführung auf der Fahrbahn.
   */
  LANE,
  /**
   * Einbahnstraße ohne eigenen Radweg, die für Radfahrer in Gegenrichtung geöffnet ist. Wird häufig
   * in Kombination mit den tags oneway=yes und oneway:bicycle=no benutzt.
   */
  OPPOSITE,
  /**
   * Ein Radfahrstreifen entgegen der Fahrrichtung einer Einbahnstraße.
   */
  OPPOSITE_LANE,
  /**
   * baulich abgesetzter Radweg neben der Fahrbahn.
   */
  TRACK,
  /**
   * baulich abgesetzter Radweg, z.B. durch einen Bordstein, entgegen der Fahrrichtung einer
   * Einbahnstraße.
   */
  OPPOSITE_TRACK,
  /**
   * Busspuren, die von Radfahrern genutzt werden dürfen.
   */
  SHARE_BUSWAY,
  /**
   * gemeinsamer Fahrstreifen für KFZ und Fahrrad.
   */
  SHARED_LANE,
}
