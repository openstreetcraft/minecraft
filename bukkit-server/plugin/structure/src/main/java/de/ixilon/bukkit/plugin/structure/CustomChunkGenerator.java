// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import de.ixilon.bukkit.plugin.structure.decoration.DecorationAreaRepository;

public class CustomChunkGenerator extends ChunkGenerator {

  private final DecorationAreaRepository repository;
  
  public CustomChunkGenerator(DecorationAreaRepository repository) {
    this.repository = repository;
  }

  @Override
  public List<BlockPopulator> getDefaultPopulators(World world) {
    List<BlockPopulator> populators = new ArrayList<>();
    populators.add(new CustomBlockPopulator(repository));
    return populators;
  }

}
