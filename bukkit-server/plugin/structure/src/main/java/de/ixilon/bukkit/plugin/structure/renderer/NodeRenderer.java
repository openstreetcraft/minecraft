// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmTag;
import net.openstreetcraft.osm.geom.OsmPoint;
import net.openstreetcraft.osm.geom.Rectangle;

public abstract class NodeRenderer implements Renderer {

  private OsmNode node;
  
  @Override
  public void render(RenderImage image, RenderModel model) {
    for (OsmNode node : model.getNodes()) {
      this.node = node;
      if (!supports(node.getTag())) {
        continue;
      }
      if (isInside(model.getBoundingBox())) {
        render(image, model, node);
      }
    }
  }
  
  protected abstract void render(RenderImage image, RenderModel model, OsmNode node);

  protected abstract boolean supports(Iterable<OsmTag> tags);

  protected boolean isInside(Rectangle bbox) {
    return isInside(bbox, new OsmPoint(node));
  }

  private boolean isInside(Rectangle bbox, OsmPoint point) {
    return point.isInside(bbox);
  }

  protected OsmNode getNode() {
    return node;
  }
}
