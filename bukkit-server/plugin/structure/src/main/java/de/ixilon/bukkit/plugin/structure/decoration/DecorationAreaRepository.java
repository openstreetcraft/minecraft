// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.decoration;

import java.util.List;

import org.bukkit.Bukkit;

import com.esri.core.geometry.Envelope2D;
import com.google.common.collect.ImmutableList;

import de.ixilon.bukkit.plugin.structure.block.BukkitMaterial;
import de.ixilon.bukkit.plugin.structure.block.UnknownMaterialException;
import de.ixilon.bukkit.plugin.structure.renderer.RenderModel;
import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.service.Cache;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.api.service.OverpassService;
import net.openstreetcraft.osm.geom.Rectangle;
import net.openstreetcraft.osm.util.OsmUtils;

public class DecorationAreaRepository {

  private final OverpassService overpassService;
  private final CoordinatesService coordinatesService;
  private final Cache<DecorationArea, List<MinecraftLocation>> minecraftLocationCache;
  private final Cache<DecorationArea, List<SphericalLocation>> sphericalLocationCache;
  private final Cache<DecorationArea, Osm> osmCache;
  private final Cache<DecorationArea, RenderModel> renderModelCache;

  /**
   * Cache subsequent requests of the same data.  
   */
  public DecorationAreaRepository(OverpassService overpassService,
      CoordinatesService coordinatesService) {
    this(overpassService, coordinatesService, cacheSize());
  }

  /**
   * Cache subsequent requests of the same data.  
   */
  public DecorationAreaRepository(OverpassService overpassService,
      CoordinatesService coordinatesService, int cacheSize) {
    this.overpassService = overpassService;
    this.coordinatesService = coordinatesService;
    this.minecraftLocationCache = new Cache<>(cacheSize);
    this.sphericalLocationCache = new Cache<>(cacheSize);
    this.osmCache = new Cache<>(cacheSize);
    this.renderModelCache = new Cache<>(cacheSize);
  }

  private static int cacheSize() {
    int width = Bukkit.getViewDistance() * 2 + 1;
    return Bukkit.getMaxPlayers() * width * width;
  }

  public List<MinecraftLocation> getMinecraftLocations(DecorationArea area) {
    return minecraftLocationCache.computeIfAbsent(area, k -> createMinecraftLocations(k));
  }

  private static List<MinecraftLocation> createMinecraftLocations(DecorationArea area) {
    int minx = area.getCenterX() - DecorationArea.DECORATION_RADIUS;
    int maxx = area.getCenterX() + DecorationArea.DECORATION_RADIUS;
    int minz = area.getCenterZ() - DecorationArea.DECORATION_RADIUS;
    int maxz = area.getCenterZ() + DecorationArea.DECORATION_RADIUS;

    ImmutableList.Builder<MinecraftLocation> locations = ImmutableList.builder();

    for (int z = minz; z <= maxz; z++) {
      for (int x = minx; x <= maxx; x++) {
        locations.add(new MinecraftLocation(x, z));
      }
    }

    return locations.build();
  }

  public List<SphericalLocation> getSphericalLocations(DecorationArea area) {
    return sphericalLocationCache.computeIfAbsent(area, k -> createSphericalLocations(k));
  }

  private List<SphericalLocation> createSphericalLocations(DecorationArea area) {
    return coordinatesService.getSphericalLocations(getMinecraftLocations(area), World.of("earth"));
  }

  public Osm getOsm(DecorationArea area) {
    return osmCache.computeIfAbsent(area, k -> createOsm(k));
  }

  private Osm createOsm(DecorationArea area) {
    return overpassService.getOsm(getBoundingBox(area));
  }

  /**
   * Bounding box of decoration area. 
   */
  public Envelope2D getBoundingBox(DecorationArea area) {
    double xmin = Double.POSITIVE_INFINITY;
    double ymin = Double.POSITIVE_INFINITY;
    double xmax = Double.NEGATIVE_INFINITY;
    double ymax = Double.NEGATIVE_INFINITY;

    for (SphericalLocation location : getSphericalLocations(area)) {
      xmin = Math.min(xmin, location.getLongitude());
      xmax = Math.max(xmax, location.getLongitude());
      ymin = Math.min(ymin, location.getLatitude());
      ymax = Math.max(ymax, location.getLatitude());
    }

    return new Envelope2D(xmin, ymin, xmax, ymax);
  }

  public RenderModel getRenderModel(DecorationArea area) {
    return renderModelCache.computeIfAbsent(area, k -> createRenderModel(k));
  }

  private RenderModel createRenderModel(final DecorationArea area) {
    final Envelope2D envelope = getBoundingBox(area);
    final double length = 2 * DecorationArea.DECORATION_RADIUS;
    final double dx = (envelope.xmax - envelope.xmin) / length;
    final double dz = (envelope.ymax - envelope.ymin) / length;
    final double degreePerMeter = (dx + dz) / 2;

    return new RenderModel() {
      @Override
      public Iterable<OsmNode> getNodes() {
        return OsmUtils.getNodes(getOsm(area));
      }

      @Override
      public Iterable<OsmWay> getWays() {
        return OsmUtils.getWays(getOsm(area));
      }

      @Override
      public Rectangle getBoundingBox() {
        return toRectangle(envelope);
      }

      @Override
      public Rectangle getBoundingBox(MinecraftLocation location) {
        return getBoundingBox(toSphericalLocation(location));
      }

      private Rectangle getBoundingBox(SphericalLocation location) {
        return Rectangle.builder()
            .xmin(location.getLongitude())
            .ymin(location.getLatitude())
            .xmax(location.getLongitude() + dx)
            .ymax(location.getLatitude() + dz)
            .build();
      }

      private Rectangle toRectangle(Envelope2D envelope) {
        return Rectangle.builder().xmin(envelope.xmin).xmax(envelope.xmax).ymin(envelope.ymin)
            .ymax(envelope.ymax).build();
      }

      private SphericalLocation toSphericalLocation(MinecraftLocation location) {
        int index = getMinecraftLocations(area).indexOf(location);
        return getSphericalLocations(area).get(index);
      }

      @Override
      public int getHeight(MinecraftLocation location) {
        int x = toInt(location.getX());
        int z = toInt(location.getZ());
        for (int y = 255; y > 0; y--) {
          try {
            if (BukkitMaterial.parse(area.getBlock(x, y, z)).isGround()) {
              return y;
            }
          } catch (UnknownMaterialException e) {
            // ignore
          }
        }
        return 0;
      }

      private int toInt(long value) {
        return Math.toIntExact(value);
      }

      @Override
      public double toDegree(double meter) {
        return meter * degreePerMeter;
      }

      @Override
      public double toMeter(double degree) {
        return degree / degreePerMeter;
      }

      @Override
      public void traverse(Visitor visitor) {
        int maxx = area.getCenterX() + DecorationArea.DECORATION_RADIUS;
        int maxz = area.getCenterZ() + DecorationArea.DECORATION_RADIUS;
        for (MinecraftLocation location : getMinecraftLocations(area)) {
          if (location.getX() < maxx && location.getZ() < maxz) {
            visitor.visit(location);
          }
        }
      }
    };
  }
}
