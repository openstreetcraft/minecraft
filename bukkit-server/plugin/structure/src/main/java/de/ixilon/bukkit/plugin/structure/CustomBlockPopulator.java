// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;

import de.ixilon.bukkit.plugin.structure.decoration.Decoration;
import de.ixilon.bukkit.plugin.structure.decoration.DecorationArea;
import de.ixilon.bukkit.plugin.structure.decoration.DecorationAreaRepository;
import de.ixilon.bukkit.plugin.structure.decoration.FundamentDecoration;
import de.ixilon.bukkit.plugin.structure.decoration.HighwayDecoration;

public class CustomBlockPopulator extends BlockPopulator {

  private final List<Decoration> decorations = new ArrayList<>();
  
  public CustomBlockPopulator(DecorationAreaRepository repository) {
    decorations.add(new FundamentDecoration(repository));
    decorations.add(new HighwayDecoration(repository));
  }

  @Override
  public void populate(World world, Random random, Chunk chunk) {
    DecorationArea area = new DecorationArea(chunk);
    for (Decoration decoration : decorations) {
      decoration.decorate(area, random);
    }
  }

}
