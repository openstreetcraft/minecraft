// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.osm.geom.OsmPolygon;
import net.openstreetcraft.osm.geom.Rectangle;

public abstract class PolygonRenderer extends WayRenderer {

  private OsmPolygon polygon;
  
  @Override
  protected boolean isInside(Rectangle bbox) {
    return getPolygon().isInside(bbox);
  }

  @Override
  protected void setWay(OsmWay way) {
    this.polygon = null;
    super.setWay(way);
  }

  private OsmPolygon getPolygon() {
    if (polygon == null) {
      polygon = new OsmPolygon(getWay(), getNodes());
    }
    return polygon;
  }
  
}
