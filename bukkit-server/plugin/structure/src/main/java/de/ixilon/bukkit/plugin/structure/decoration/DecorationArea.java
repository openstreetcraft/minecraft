// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.decoration;

import java.util.Objects;

import org.bukkit.Chunk;
import org.bukkit.Material;

public class DecorationArea implements Comparable<DecorationArea> {
  public static int DECORATION_RADIUS = 8;
  
  private final Chunk chunk;

  public DecorationArea(Chunk chunk) {
    this.chunk = chunk;
  }
  
  public void setBlock(int blockX, int blockY, int blockZ, Material material) {
    chunk.getBlock(blockX, blockY, blockZ).setType(material);
  }

  public Material getBlock(int blockX, int blockY, int blockZ) {
    return chunk.getBlock(blockX, blockY, blockZ).getType();
  }

  @Override
  public int hashCode() {
    return Objects.hash(chunk.getX(), chunk.getX());
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof DecorationArea) {
      return compareTo((DecorationArea) other) == 0;
    }
    return false;
  }

  @Override
  public int compareTo(DecorationArea other) {
    int cmp = Integer.compare(chunk.getX(), other.chunk.getX());
    return (cmp != 0) ? cmp : Integer.compare(chunk.getZ(), other.chunk.getZ());
  }

  public int getCenterX() {
    return chunk.getX() << 4 + DECORATION_RADIUS;
  }

  public int getCenterZ() {
    return chunk.getZ() << 4 + DECORATION_RADIUS;
  }

}
