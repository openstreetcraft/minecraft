// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.feature;

/**
 * Oberfläche eines Weges oder einer Fläche.
 */
public enum Surface {
  /**
   * Versiegelte Oberfläche: Eine grobe Beschreibung für Wege, deren Oberfläche z.B. Asphalt, Beton
   * oder Pflaster versiegelt ist.
   */
  PAVED,

  /**
   * Ohne Straßenbelag: Eine grobe Beschreibung für Wege, deren Oberfläche nicht mit z.B. Asphalt,
   * Beton oder Pflaster versiegelt ist.
   */
  UNPAVED,

  /**
   * Asphalt.
   */
  ASPHALT,

  /**
   * Beton. Eine große, zusammenhängende und geschlossene Oberfläche aus Beton.
   */
  CONCRETE,

  /**
   * Betonspurplatten. In der Mitte findet man einen Streifen aus Sand oder Gras.
   */
  CONCRETE_LANES,

  /**
   * Betonplatten ohne große Sand- oder Grasflächen zwischen den einzelnen Platten.
   */
  CONCRETE_PLATES,

  /**
   * Pflastersteine meist aus Beton gefertigt mit gleichmäßiger, regelmäßiger Form. Sie haben daher
   * bei der Verlegung nur schmale Zwischenräume. Oft in Fußgängerzonen oder auf Wegen mit geringenn
   * Autoverkehr. Sie sind nicht für schwere Belastungen ausgelegt.
   */
  PAVING_STONES,

  /**
   * Pflaster aus unbehauenem Naturstein. Die Oberfläche ist gerundet, es gibt keine vorgegebene
   * Form. Damit sind Flächen uneben, schlecht zu gehen oder zu befahren. In Deutschland wird
   * cobblestone wegen der gerundeten Oberfläche auch für Kopfsteinpflaster verwendet.
   */
  COBBLESTONE,

  /**
   * Steinpflaster mit abgeflachten Steinen, angenehmer zu gehen oder zu befahren als
   * Kopfsteinpflaster. Steingröße: 15 cm bis 20 cm.
   */
  COBBLESTONE_FLATTENED,

  /**
   * Metall: Findet zum Beispiel bei Brücken Verwendung. Ebenso werden temporäre Straßen bei weichem
   * Untergrund (z.B. Ersatzweg bei Baustellen) damit erstellt.
   */
  METAL,

  /**
   * Holz: Findet zum Beispiel bei Brücken, Stegen und hölzernen Wegen (durch sumpfiges Gelände)
   * Verwendung.
   */
  WOOD,

  /**
   * Eine befestigte und verdichtete Deckschicht für Straßen und Wege, die aus einem gebrochenen
   * Natursteinmaterial besteht (regional auch als Grant oder Makadam bezeichnet). Benutzt für
   * Parkwege, bessere Wirtschaftswege oder auch für service.
   */
  COMPACTED,

  /**
   * Gras, Rasen, Wiese: Durch den Bewuchs weniger der Erosion ausgesetzt als dirt, earth, ground.
   * Nur für geringe Belastung (Fußgänger) geeignet.
   */
  GRASS,

  /**
   * Rasengittersteine: Um eine Fläche zu befestigen, ohne sie vollständig zu versiegeln, werden
   * Gitter aus Beton oder Kunststoff verlegt, die einerseits die Tragfähigkeit herstellen und
   * andererseits durch die offenen Stellen die Versickerung ermöglichen. Werden oft für Parkplätze
   * und für Rettungswege verwendet.
   */
  GRASS_PAVER,

  /**
   * Loser Schotter: Scharfkantig gebrochener Naturstein.
   */
  GRAVEL,

  /**
   * Schotterrasen: Eine meist 30cm dicke Schicht aus Schotter und Erde. Die mit Gräser und Kräutern
   * bewachsene Oberfläche wird für Park-, Zelt- oder Festplätze verwendet, da sie grün,
   * wasserdurchlässig und gleichzeitig für Fahrzeuge befahrbar ist.
   */
  GRAVEL_TURF,

  /**
   * Fester Splitt oder Kies. Mehrschichtige Befestigung mit einer festen Splitt, Basalt-, Quarzit-
   * oder anderer granularer Deckschicht auf einer tragenden Schotterdecke. Gut begehbar für
   * Fußgänger, Jogger, Reiter oder zum Radfahren.
   */
  FINE_GRAVEL,

  /**
   * Eisstraße, Verkehrswege, die über zugefrorene Seen, Flüsse oder Meeresgewässer führen.
   */
  ICE,

  /**
   * Loser Kies: Durch natürliche Erosion in (früheren) Fließgewässern abgerundete Steine
   * unterschiedlicher Größe (ca. 5-50mm).
   */
  PEBBLESTONE,

  /**
   * gewachsene, naturbelassene Oberfläche, man kann Trampelpfade von Menschen oder Tieren erkennen.
   */
  GROUND,

  /**
   * nackte, unbedeckte Oberfläche, man kann Trampelpfade von Menschen oder Tieren erkennen.
   */
  EARTH,

  /**
   * Unbefestigte Straße: Ist der Erosion schutzlos ausgeliefert und daher oft uneben.
   */
  DIRT,

  /**
   * Matsch, Morast, Schlamm, Schlick. Der Feuchtigkeitsgrad eines Weges ist meistens jahreszeitlich
   * unterschiedlich und wetterabhängig.
   */
  MUD,

  /**
   * Salt lakes.
   */
  SALT,

  /**
   * Loser Sand: Durch natürliche Erosion in (früheren) Fließgewässern sehr klein gemahlene Steine.
   * Die Korngröße liegt um 1mm.
   */
  SAND,

  /**
   * Winter roads.
   */
  SNOW,

  /**
   * Holzschnitzel: Woodchips as a surface.
   */
  WOODCHIPS,

  /**
   * Tartan- oder Kunststoffbelag ist ein wetterfester Belag aus Kunststoff, wie er oft für
   * Leichtathletik- und sonstige Sportanlagen verwendet wird, meist Tartanbelag genannt.
   */
  TARTAN,

  /**
   * Kunstrasen: Eine aus synthetischen Fasern hergestellte Oberfläche, die wie Rasen aussieht.
   */
  ARTIFICAL_TURF,

  /**
   * DecoTurf is an artificial surface used for some tennis courts.
   */
  DECOTURF,

  /**
   * Clay wird bei Ascheplätzen (z.B. Tennis- oder Fussballplätzen) verwendet.
   */
  CLAY,

  /**
   * Metal grids are often used as a surface on industrial-style bridges or stairs. When wet, the
   * surface can become very slippery, especially for bikes. The surface can also be unsuitable for
   * dogs, due to the sharp edges and the look-through effect.
   */
  METALL_GRID,

  /**
   * Behauenes Steinpflaster (Pflastersteine): Diese Steine sind ebener als (surface=cobblestone)
   * oder Kopfsteinpflaster und haben annähernd eine Quaderform. Relativ glatte, ungefähr
   * rechteckige Steine, die in regelmäßigen Mustern verlegt werden.
   */
  SETT,

}
