// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.bukkit.plugin.structure.block.Material;
import de.ixilon.bukkit.plugin.structure.block.UnknownMaterialException;

public final class RenderImageUtil {

  /**
   * Remove vegetation, like grass above a location.
   */
  public static void removeVegetation(RenderImage image, long x, long y, long z) {
    final Material material;
    try {
      material = image.getMaterial(x, y, z);
    } catch (UnknownMaterialException exception) {
      return;
    }
    if (material.isFlower()) {
      image.setMaterial(x, y, z, Material.AIR);
      if (material.isTallFlower()) {
        image.setMaterial(x, y + 1, z, Material.AIR);
      }
    }
  }

  /**
   * Remove tree leaves.
   */
  public static void removeTreeLeave(RenderImage image, long x, long y, long z) {
    final Material material;
    try {
      material = image.getMaterial(x, y, z);
    } catch (UnknownMaterialException exception) {
      return;
    }
    if (material.isTreeLeave()) {
      image.setMaterial(x, y, z, Material.AIR);
    }
  }
  
  /**
   * Checks if it is a leaf and if a 2 block radius does not contain a valid log or 5 unexpected
   * blocks.
   */
  @Deprecated // unused
  private static boolean isLonelyLeave(RenderImage image, long x, long y, long z) {
    Material material;
    try {
      material = image.getMaterial(x, y, z);
    } catch (UnknownMaterialException exception) {
      return false;
    }
    if (!material.isTreeLeave()) {
      return false;
    }
    int unexpected = 0;
    for (int dx = -2; dx <= 2; dx++) {
      for (int dy = -2; dy <= 2; dy++) {
        for (int dz = -2; dz <= 2; dz++) {
          try {
            material = image.getMaterial(x + dx, y + dy, z + dz);
          } catch (UnknownMaterialException exception) {
            unexpected++;
            continue;
          }
          if (material.isTreeLog()) {
            return false;
          } else if (material.isTreeLeave()) {
            continue;
          }
          if (unexpected++ >= 5) {
            return false;
          }
        }
      }
    }
    return true;
  }
}
