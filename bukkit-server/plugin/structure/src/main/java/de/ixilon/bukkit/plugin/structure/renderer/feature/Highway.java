// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.feature;

public enum Highway {
  /**
   * Autobahn. Straße mit baulich getrennten Fahrtrichtungen (im allgemeinen Grünstreifen) und
   * besonderen Nutzungsbeschränkungen. Typischerweise zwei Fahrstreifen je Fahrtrichtung oder mehr
   * und ein Standstreifen.
   */
  MOTORWAY, MOTORWAY_LINK,

  /**
   * Autobahnähnliche Straße, Kraftfahrstraße, Autostrasse, Schnellstraße.
   */
  TRUNK, TRUNK_LINK,

  /**
   * Straßen, die von nationaler Bedeutung sind und Zentren miteinander verbinden, z.B.
   * Bundesstraßen.
   */
  PRIMARY, PRIMARY_LINK,

  /**
   * Straßen, die von überregionaler Bedeutung sind und kleinere Zentren miteinander verbinden. In
   * Deutschland Landes-, (Staats-,) oder sehr gut ausgebaute Kreisstraße. Straße mit Mittellinie,
   * die kleinere Städte oder größere Orte verbindet.
   */
  SECONDARY, SECONDARY_LINK,

  /**
   * Straßen, die Dörfer untereinander verbinden bzw. innerstädtische Straßen mit
   * Durchfahrtscharakter. In Deutschland Kreisstraße, sehr gut ausgebaute
   * Gemeindeverbindungsstraße. Allgemein auch: Innerstädtische Vorfahrtstraßen mit
   * Durchfahrtscharakter.
   */
  TERTIARY, TERTIARY_LINK,

  /**
   * Öffentlich befahrbare Nebenstraßen mit einfachstem Ausbauzustand, typischerweise keine
   * Mittellinie. Z. B. Gemeindestraße mit Verbindungscharakter.
   */
  UNCLASSIFIED,

  /**
   * Straße an und in Wohngebieten, die keiner anderen Straßenklasse angehört. Hat immer zwei
   * Fußwege.
   */
  RESIDENTIAL,

  /**
   * Erschließungsweg zu oder innerhalb von Einrichtungen wie Sportanlagen, Stränden,
   * Autobahnraststätten oder allgemein zu Gebäuden. Wird auch für den Zugang zu Parkplätzen oder
   * Recyclinghöfen benutzt.
   */
  SERVICE,

  /**
   * Verkehrsberuhigter Bereich, Wohnstraße, Spielstraße.
   */
  LIVING_STREET,

  /**
   * Weg, Platz oder Straße auf der nur Fußgänger erlaubt sind (z. B. Fußgängerzone).
   */
  PEDESTRIAN,

  /**
   * Wirtschafts-, Feld- oder Waldweg.
   */
  TRACK,

  /**
   * Spurbus-Strecke, auf der nur spezielle Fahrzeuge (z. B. Busse) mit Spurführungseinrichtung
   * fahren können.
   */
  BUS_GUIDEWAY,

  /**
   * Notbremsweg, findet man vor gefährdeten Ortseinfahrten, an Bergabstrecken.
   */
  ESCAPE,

  /**
   * Rennstrecke.
   */
  RACEWAY,

  /**
   * Straße unbekannter Klassifikation.
   */
  ROAD,

  /**
   * Fußweg, zur ausschließlichen Benutzung durch Fußgänger geeignet.
   */
  FOOTWAY,

  /**
   * Reitweg.
   */
  BRIDLEWAY,

  /**
   * Treppen.
   */
  STEPS,

  /**
   * Wanderwege oder Trampelpfade.
   */
  PATH,

  /**
   * Radweg.
   */
  CYCLEWAY,
}
