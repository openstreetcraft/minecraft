// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import java.util.Random;

import de.ixilon.bukkit.plugin.structure.block.Material;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmTag;
import net.openstreetcraft.osm.util.OsmNodeUtils;
import net.openstreetcraft.osm.util.OsmTagUtils;

/**
 * A single tree.
 */
public class TreeRenderer extends NodeRenderer {

  @Override
  protected boolean supports(Iterable<OsmTag> tags) {
    return OsmTagUtils.isTag(tags, "natural", "tree");
  }

  @Override
  protected void render(final RenderImage image, final RenderModel model, final OsmNode node) {
    final Random random = OsmNodeUtils.createRandom(node);
    model.traverse(location -> {
      if (isInside(model.getBoundingBox(location))) {
        int y = model.getHeight(location) + 1;
        Material material = getTreeByTag(node.getTag(), random);
        image.setMaterial(location.getX(), y, location.getZ(), material);
      }
    });
  }

  private static Material getTreeByTag(Iterable<OsmTag> tags, Random random) {
    switch (OsmTagUtils.getTag(tags, "genus").or("").toLowerCase()) {
      case "betula":
        return Material.BIRCH_SAPLING;
      default:
        // ignore
    }
    switch (OsmTagUtils.getTag(tags, "leaf_type").or("").toLowerCase()) {
      case "broadleaved":
        return Material.OAK_SAPLING;
      case "needleleaved":
        return Material.SPRUCE_SAPLING;
      default:
        switch (random.nextInt(3)) {
          case 0:
            return Material.OAK_SAPLING;
          case 1:
            return Material.SPRUCE_SAPLING;
          case 2:
            return Material.BIRCH_SAPLING;
          default:
            // ignore
        }
    }
    return Material.OAK_SAPLING;
  }

}
