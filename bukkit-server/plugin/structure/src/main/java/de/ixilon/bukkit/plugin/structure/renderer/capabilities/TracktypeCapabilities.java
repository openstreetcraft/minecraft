// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.capabilities;

import java.util.Random;

import de.ixilon.bukkit.plugin.structure.renderer.feature.Surface;
import de.ixilon.bukkit.plugin.structure.renderer.feature.Tracktype;

public class TracktypeCapabilities implements RoadwayCapabilities {

  private final Random random;

  private final double width;
  private final SurfaceCapabilities surface;

  private final double middleWidth;
  private final SurfaceCapabilities middleSurface;

  private final double sideWidth;
  private final SurfaceCapabilities sideSurface;

  /**
   * Rendering capabilities of track type.
   */
  public TracktypeCapabilities(Tracktype tracktype, Random random) {
    this.random = random;

    double width = 5;
    Surface surface = Surface.ASPHALT;

    double sideWidth = 0;
    Surface sideSurface = Surface.GRAVEL;

    double middleWidth = 0;
    Surface middleSurface = Surface.GRASS;

    switch (tracktype) {
      case GRADE1:
        surface = random(Surface.ASPHALT, Surface.CONCRETE, Surface.GRAVEL);
        break;
      case GRADE2:
        surface = Surface.GRAVEL;
        break;
      case GRADE3:
        width = 4;
        surface = random(Surface.GRAVEL, Surface.SAND, Surface.DIRT);
        break;
      case GRADE4:
        width = 3;
        middleWidth = 1;
        surface = random(Surface.GRASS, Surface.SAND, Surface.DIRT);
        break;
      case GRADE5:
        width = 3;
        surface = random(Surface.GRASS, Surface.SAND, Surface.DIRT);
        break;
      default:
        throw new IllegalArgumentException(tracktype.toString());
    }

    this.width = width;
    this.surface = new SurfaceCapabilities(surface);

    this.middleWidth = middleWidth;
    this.middleSurface = new SurfaceCapabilities(middleSurface);

    this.sideWidth = sideWidth;
    this.sideSurface = new SurfaceCapabilities(sideSurface);
  }

  private Surface random(Surface... surfaces) {
    return surfaces[random.nextInt(surfaces.length)];
  }

  @Override
  public double getWidth() {
    return width;
  }

  @Override
  public SurfaceCapabilities getSurfaceCapabilities() {
    return surface;
  }

  @Override
  public double getMiddleWidth() {
    return middleWidth;
  }

  @Override
  public SurfaceCapabilities getMiddleSurfaceCapabilities() {
    return middleSurface;
  }

  @Override
  public double getSideWidth() {
    return sideWidth;
  }

  @Override
  public SurfaceCapabilities getSideSurfaceCapabilities() {
    return sideSurface;
  }

}
