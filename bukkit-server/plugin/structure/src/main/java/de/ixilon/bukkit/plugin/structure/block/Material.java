// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.block;

public enum Material {
  AIR,
  ACACIA_LEAVES,
  ACACIA_LOG,
  ALLIUM,
  ANDESITE,
  AZURE_BLUET,
  BEDROCK,
  BIRCH_LEAVES,
  BIRCH_LOG,
  BIRCH_SAPLING,
  BLUE_ORCHID,
  CLAY,
  COBBLESTONE,
  CORNFLOWER,
  DANDELION,
  DARK_OAK_LEAVES,
  DARK_OAK_LOG,
  DARK_OAK_WOOD,
  DIORITE,
  DIRT,
  GLASS,
  GRANITE,
  GRASS,
  GRASS_BLOCK,
  GRASS_PATH,
  GRAVEL,
  ICE,
  IRON_BLOCK,
  JUNGLE_LEAVES,
  JUNGLE_LOG,
  LILAC,
  LILY_OF_THE_VALLEY,
  OAK_LEAVES,
  OAK_LOG,
  OAK_SAPLING,
  ORANGE_TULIP,
  OXEYE_DAISY,
  PEONY,
  PINK_TULIP,
  POLISHED_DIORITE,
  POPPY,
  RED_CARPET,
  RED_TULIP,
  ROSE_BUSH,
  SAND,
  SNOW,
  SPRUCE_LEAVES,
  SPRUCE_LOG,
  SPRUCE_SAPLING,
  STONE,
  SUNFLOWER,
  TALL_GRASS,
  WITHER_ROSE,
  WHITE_CONCRETE,
  WHITE_TULIP;

  /**
   * Is this material on the surface. 
   */
  public boolean isGround() {
    switch (this) {
      case BEDROCK:
      case COBBLESTONE:
      case DIRT:
      case GRAVEL:
      case GRASS_BLOCK:
      case SAND:
      case STONE:
        return true;
      default:
        return false;
    }
  }
  
  public boolean isFlower() {
    return isTallFlower() || isSmallFlower();
  }
  
  /**
   * Is this material a small one block high flower. 
   */
  public boolean isSmallFlower() {
    switch (this) {
      case ALLIUM:
      case AZURE_BLUET:
      case BLUE_ORCHID:
      case CORNFLOWER:
      case DANDELION:
      case GRASS:
      case LILY_OF_THE_VALLEY:
      case ORANGE_TULIP:
      case OXEYE_DAISY:
      case PINK_TULIP:
      case POPPY:
      case RED_TULIP:
      case WHITE_TULIP:
      case WITHER_ROSE:
        return true;
      default:
        return false;
    }
  }
  
  /**
   * Is this material a tall two block high flower. 
   */
  public boolean isTallFlower() {
    switch (this) {
      case LILAC:
      case PEONY:
      case ROSE_BUSH:
      case SUNFLOWER:
      case TALL_GRASS:
        return true;
      default:
        return false;
    }
  }
  
  public boolean isTree() {
    return isTreeLog() || isTreeLeave();
  }
  
  public boolean isTreeLog() {
    return is("_log");
  }

  public boolean isTreeLeave() {
    return is("_leaves");
  }

  private boolean is(String name) {
    return name().toLowerCase().contains(name);
  }
}
