// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.osm.geom.Rectangle;

/**
 * Provides data about the drawing area. 
 */
public interface RenderModel {

  /**
   * Collection of OpenStreetMap nodes within drawing area.
   */
  Iterable<OsmNode> getNodes();

  /**
   * Collection of OpenStreetMap ways within drawing area.
   */
  Iterable<OsmWay> getWays();

  /**
   * Bounding box of drawing area (entire chunk).
   */
  Rectangle getBoundingBox();

  /**
   * Bounding box of single block at Minecraft block coordinate.
   */
  Rectangle getBoundingBox(MinecraftLocation location);

  /**
   * Heighest block at Minecraft block coordinate.
   */
  int getHeight(MinecraftLocation location);

  double toDegree(double meter);

  double toMeter(double degree);

  /**
   * Traverse every Minecraft block within drawing area. 
   */
  void traverse(Visitor visitor);

  interface Visitor {
    void visit(MinecraftLocation location);
  }
}
