// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.osm.schema.OsmTag;
import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.osm.geom.OsmNodeList;
import net.openstreetcraft.osm.geom.Rectangle;
import net.openstreetcraft.osm.util.OsmWayUtils;

public abstract class WayRenderer implements Renderer {

  private OsmNodeList nodes;
  private OsmWay way;

  @Override
  public void render(RenderImage image, RenderModel model) {
    nodes = OsmNodeList.copyOf(model.getNodes());
    for (OsmWay way : model.getWays()) {
      setWay(way);
      if (!supports(OsmWayUtils.getTags(way))) {
        continue;
      }
      if (isInside(model.getBoundingBox())) {
        render(image, model, way);
      }
    }
  }

  protected abstract void render(RenderImage image, RenderModel model, OsmWay way);

  protected abstract boolean isInside(Rectangle bbox);

  protected abstract boolean supports(Iterable<OsmTag> tags);

  protected OsmNodeList getNodes() {
    return nodes;
  }

  protected OsmWay getWay() {
    return way;
  }

  protected void setWay(OsmWay way) {
    this.way = way;
  }
}
