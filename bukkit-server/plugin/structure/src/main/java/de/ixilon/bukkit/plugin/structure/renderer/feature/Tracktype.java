// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.feature;

/**
 * Unterhaltungszustand, v.a. im Bezug auf die Oberflächenfestigkeit, von Wirtschafts-, Feld- oder
 * Waldwegen angegeben.
 */
public enum Tracktype {
  /**
   * Wasserfester Belag (Asphalt, Beton oder Pflastersteine).
   */
  GRADE1,

  /**
   * Wassergebundene Decke (Schotter oder andere verdichtete Materialien).
   */
  GRADE2,

  /**
   * Befestigter oder ausgebesserter Weg, oft ohne Unterbau, mit teils relativ fester, teils weicher
   * Oberfläche (z. B. Feinschotter-, Sand- oder Erdweg).
   */
  GRADE3,

  /**
   * Unbefestigter Weg, hauptsächlich weiche Materialien, Pflanzenwuchs entlang der Spurmitte (z. B.
   * Gras-, Sand- oder Erdweg).
   */
  GRADE4,

  /**
   * Unbefestigter Weg, Oberfläche bestehend aus weichen Materialien, z.B. Gras, Sand, Erde etc.
   */
  GRADE5,

}
