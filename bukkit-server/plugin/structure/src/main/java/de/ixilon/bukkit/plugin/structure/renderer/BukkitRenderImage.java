// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.bukkit.plugin.structure.block.BukkitMaterial;
import de.ixilon.bukkit.plugin.structure.block.Material;
import de.ixilon.bukkit.plugin.structure.block.UnknownMaterialException;
import de.ixilon.bukkit.plugin.structure.decoration.DecorationArea;

public class BukkitRenderImage implements RenderImage {
  private final DecorationArea area;

  public BukkitRenderImage(DecorationArea area) {
    this.area = area;
  }

  @Override
  public void setMaterial(long x, long y, long z, Material material) {
    try {
      area.setBlock(toInt(x), toInt(y), toInt(z), BukkitMaterial.of(material));
    } catch (UnknownMaterialException exception) {
      // ignore unknown material
    }
  }

  @Override
  public Material getMaterial(long x, long y, long z) throws UnknownMaterialException {
    return BukkitMaterial.parse(area.getBlock(toInt(x), toInt(y), toInt(z)));
  }

  private static int toInt(long x) {
    return Math.toIntExact(x);
  }
}
