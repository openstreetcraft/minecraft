// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import java.util.Random;

import com.google.common.base.Optional;

import de.ixilon.bukkit.plugin.structure.renderer.capabilities.HighwayCapabilities;
import de.ixilon.bukkit.plugin.structure.renderer.capabilities.RoadwayCapabilities;
import de.ixilon.bukkit.plugin.structure.renderer.capabilities.TracktypeCapabilities;
import de.ixilon.bukkit.plugin.structure.renderer.feature.Highway;
import de.ixilon.bukkit.plugin.structure.renderer.feature.Sidewalk;
import de.ixilon.bukkit.plugin.structure.renderer.feature.Surface;
import de.ixilon.bukkit.plugin.structure.renderer.feature.Tracktype;
import de.ixilon.osm.schema.OsmTag;
import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.osm.geom.Rectangle;
import net.openstreetcraft.osm.util.OsmTagUtils;
import net.openstreetcraft.osm.util.OsmWayUtils;

public class HighwayRenderer extends PolylineRenderer {

  private Optional<Highway> highway;
  private Optional<Surface> surface;
  private Optional<String> lanes;
  private Optional<Sidewalk> sidewalk;
  private Optional<Tracktype> tracktype;

  @Override
  protected boolean supports(Iterable<OsmTag> tags) {
    if (OsmTagUtils.isTag(tags, "highway") && !OsmTagUtils.isTag(tags, "tunnel")) {
      highway = OsmTagUtils.getTag(tags, "highway", Highway.class);
      surface = OsmTagUtils.getTag(tags, "surface", Surface.class);
      lanes = OsmTagUtils.getTag(tags, "lanes");
      sidewalk = OsmTagUtils.getTag(tags, "sidewalk", Sidewalk.class);
      tracktype = OsmTagUtils.getTag(tags, "tracktype", Tracktype.class);
      return true;
    }
    return false;
  }

  @Override
  protected void render(final RenderImage image, final RenderModel model, final OsmWay way) {

    final Random random = OsmWayUtils.createRandom(way);
    final RoadwayCapabilities roadway;
    
    if (tracktype.isPresent() && (highway.get() == Highway.TRACK)) {
      roadway = new TracktypeCapabilities(tracktype.get(), random);
    } else {
      if (!highway.isPresent()) {
        return;
      }

      HighwayCapabilities highway = new HighwayCapabilities(this.highway.get(), random);

      if (sidewalk.isPresent()) {
        switch (sidewalk.get()) {
          case BOTH:
            highway.setSidewalk(true);
            break;
          default:
            highway.setSidewalk(false);
            break;
        }
      }

      if (lanes.isPresent()) {
        try {
          highway.setLanes(Integer.parseInt(lanes.get()));
        } catch (NumberFormatException exception) {
          // ignore
        }
      }

      if (surface.isPresent()) {
        highway.setSurface(surface.get());
      }

      roadway = highway;
    }

    setWidth(model, roadway.getWidth());

    final double middleDistance = roadway.getMiddleWidth() / 2;
    final double sideDistance = (roadway.getWidth() / 2) - roadway.getSideWidth();

    model.traverse(location -> {
      Rectangle bbox = model.getBoundingBox(location);
      if (isInside(bbox)) {
        long x = location.getX();
        long y = model.getHeight(location);
        long z = location.getZ();

        RenderImageUtil.removeVegetation(image, x, y + 1, z);
        
        for (int dx = -1; dx <= 1; dx++) {
          for (int dy = 1; dy <= 3; dy++) {
            for (int dz = -1; dz <= 1; dz++) {
              RenderImageUtil.removeTreeLeave(image, x + dx, y + dy, z + dz);
            }
          }
        }

        double distance = getDistanceInMeter(model, bbox);
        
        if (distance < middleDistance) {
          image.setMaterial(x, y, z, roadway.getMiddleSurfaceCapabilities().getMaterial());
        } else if (distance > sideDistance) {
          image.setMaterial(x, y, z, roadway.getSideSurfaceCapabilities().getMaterial());
        } else {
          image.setMaterial(x, y, z, roadway.getSurfaceCapabilities().getMaterial());
        }
      }
    });
  }

}
