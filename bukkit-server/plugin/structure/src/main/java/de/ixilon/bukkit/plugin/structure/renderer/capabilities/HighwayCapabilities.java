// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.capabilities;

import java.util.Random;

import de.ixilon.bukkit.plugin.structure.renderer.feature.Highway;
import de.ixilon.bukkit.plugin.structure.renderer.feature.Surface;

public class HighwayCapabilities implements RoadwayCapabilities {

  private final Random random;

  private SurfaceCapabilities surface;

  private final double middleWidth;
  private final SurfaceCapabilities middleSurface;

  private final double sideWidth;
  private final SurfaceCapabilities sideSurface;

  private final double laneWidth;
  private int lanes;

  private boolean isSidewalk = true;

  /**
   * Rendering capabilities of highways. 
   */
  public HighwayCapabilities(Highway highway, Random random) {
    this.random = random;

    Surface surface = Surface.ASPHALT;

    double sideWidth = 0;
    Surface sideSurface = Surface.GRAVEL;

    double middleWidth = 0;
    Surface middleSurface = Surface.GRASS;

    double laneWidth = 2.75;
    int lanes = 2;

    switch (highway) {
      case BRIDLEWAY:
        lanes = 1;
        laneWidth = 4;
        surface = random(Surface.DIRT, Surface.GRASS);
        break;
      case BUS_GUIDEWAY:
        lanes = 1;
        laneWidth = 3.5;
        break;
      case CYCLEWAY:
        lanes = 1;
        laneWidth = 2;
        break;
      case ESCAPE:
        lanes = 1;
        laneWidth = 3.5;
        surface = Surface.GRAVEL;
        break;
      case FOOTWAY:
      case PEDESTRIAN:
        lanes = 1;
        laneWidth = 2;
        surface = Surface.PAVING_STONES;
        break;
      case LIVING_STREET:
      case ROAD:
      case UNCLASSIFIED:
        lanes = 2;
        laneWidth = 2.75;
        break;
      case MOTORWAY:
        // RG 29,5
        lanes = 2;
        laneWidth = 4.0;
        sideWidth = 2.0;
        break;
      case MOTORWAY_LINK:
        lanes = 1;
        laneWidth = 4.0;
        sideWidth = 2.0;
        break;
      case PATH:
        lanes = 1;
        laneWidth = 3.0;
        surface = random(Surface.GRAVEL, Surface.GRASS);
        break;
      case PRIMARY:
        // RQ 10,5
        lanes = 2;
        laneWidth = 4.0;
        sideWidth = 2.0;
        break;
      case PRIMARY_LINK:
        lanes = 1;
        laneWidth = 4.0;
        sideWidth = 2.0;
        break;
      case RACEWAY:
        lanes = 1;
        laneWidth = 8.0;
        sideWidth = 4.0;
        break;
      case RESIDENTIAL:
        lanes = 2;
        laneWidth = 2.5;
        sideWidth = 2.0;
        sideSurface = Surface.PAVING_STONES;
        break;
      case SECONDARY:
        // RQ 9,5
        lanes = 2;
        laneWidth = 3.0;
        sideWidth = 2.0;
        break;
      case SECONDARY_LINK:
        lanes = 1;
        laneWidth = 3.0;
        sideWidth = 2.0;
        break;
      case SERVICE:
        lanes = 1;
        laneWidth = 2.75;
        break;
      case STEPS:
        lanes = 1;
        laneWidth = 3.0;
        sideSurface = Surface.COBBLESTONE;
        break;
      case TERTIARY:
        // RQ 7,5
        lanes = 2;
        laneWidth = 2.75;
        sideWidth = 1.0;
        break;
      case TERTIARY_LINK:
        lanes = 1;
        laneWidth = 2.75;
        sideWidth = 1.0;
        break;
      case TRACK:
        lanes = 1;
        laneWidth = 2.75;
        surface = Surface.GRAVEL;
        break;
      case TRUNK:
        // RQ 26
        lanes = 2;
        laneWidth = 4.0;
        sideWidth = 4.0;
        break;
      case TRUNK_LINK:
        lanes = 1;
        laneWidth = 4.0;
        sideWidth = 4.0;
        break;
      default:
        throw new IllegalArgumentException(highway.toString());
    }

    this.laneWidth = laneWidth;
    this.lanes = lanes;

    this.surface = new SurfaceCapabilities(surface);

    this.middleWidth = middleWidth;
    this.middleSurface = new SurfaceCapabilities(middleSurface);

    this.sideWidth = sideWidth;
    this.sideSurface = new SurfaceCapabilities(sideSurface);
  }

  private Surface random(Surface... surfaces) {
    return surfaces[random.nextInt(surfaces.length)];
  }

  public void setLanes(int lanes) {
    this.lanes = lanes;
  }

  public void setSidewalk(boolean isSidewalk) {
    this.isSidewalk = isSidewalk;
  }

  @Override
  public double getWidth() {
    return (isSidewalk ? 2 : 0) * sideWidth + middleWidth + lanes * laneWidth;
  }

  @Override
  public SurfaceCapabilities getSurfaceCapabilities() {
    return surface;
  }

  public void setSurface(Surface surface) {
    this.surface = new SurfaceCapabilities(surface);
  }

  @Override
  public double getMiddleWidth() {
    return middleWidth;
  }

  @Override
  public SurfaceCapabilities getMiddleSurfaceCapabilities() {
    return middleSurface;
  }

  @Override
  public double getSideWidth() {
    return sideWidth;
  }

  @Override
  public SurfaceCapabilities getSideSurfaceCapabilities() {
    return sideSurface;
  }

}
