// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer.feature;

/**
 * Alle Arten von Barrieren, die die Bewegungsfreiheit von Fahrzeugen, Menschen oder Tieren
 * einschränken.
 */
public enum Barrier {
  /**
   * A barrier which nature cannot be determined; typically only used in mapping using aerial
   * imagery.
   */
  YES,
  /**
   * Also called guard cable. This is a road side or median barrier made of steel wire ropes mounted
   * on weak posts.
   */
  CABLE_BARRIER,
  /**
   * Stadtmauer.
   */
  CITY_WALL,
  /**
   * Graben, normalerweise mit Wasser am Grund, kann zusammen mit waterway=stream oder
   * waterway=drain verwendet werden.
   */
  DITCH,
  /**
   * Zaun.
   */
  FENCE,
  /**
   * Leitplanke - Die rechte Seite des gezeichneten Barrierenverlaufs ist die innere Seite mit
   * Stützen, die linke die glatte äußere.
   */
  GUARD_RAIL,
  /**
   * Geländer - Häufig an Treppen zum Festhalten mit der Hand, aber auch an Spielfeldrändern oder
   * Platformbegrenzungen des öffentlichen Nahverkehrs.
   */
  HANDRAIL,
  /**
   * Hecke als Gartenumzäunung, auch um Felder, Windschutz, in sehr unterschiedlichen Höhen, auch
   * mit durchgewachsenen Bäumen zur Brennholzgewinnung. (Achtung: Doppeldeutigkeit bei der Nutzung
   * auf Area (closed ways); einige Anwendungen interpretieren die gesamte Fläche als Hecke; andere
   * (historisch bedingt !?) nur den Flächenrand.
   */
  HEDGE,
  /**
   * Ein Bordstein (z.B. an Bürgersteigen) stellt ein Hindernis für Fahrzeuge und Rollstuhlfahrer
   * dar. Die Höhe des Bordsteins ist wichtig, um die möglichen Benutzergruppen eingrenzen zu
   * können. Falls vorhanden, kann diese zusätzlich mit height=* markiert werden. Die rechte Seite
   * des gezeichneten Barrierenverlaufs ist unten, die linke oben.
   */
  KERB,
  /**
   * Eine Stützmauer ist ein Gebilde, das Erde oder Felsen von einem Gebäude, einem Aufbau oder
   * einer Fläche zurückhält. Stützmauern können talwärts gerichtete Bodenbewegungen oder Erosion
   * unterbinden und bieten eine Abstützung für senkrechte oder nahezu senkrechte Böschungen. Die
   * tiefe Seite ist rechts des gezeichneten Barrierenverlaufs.
   */
  RETAINING_WALL,
  /**
   * Static anti-tank obstacle defences may take a number of forms.
   */
  TANK_TRAP,
  /**
   * Mauer.
   */
  WALL,
  /**
   * Großer, stabiler Block, der nur mit schwerem Gerät oder großer Anstrengung versetzt werden
   * kann. Typischerweise aus stabilem Material wie Beton, um größere Fahrzeuge zu stoppen, oder
   * auch Naturstein.
   */
  BLOCK,
  /**
   * Ein Poller, Pfosten oder eine Bake auf einem Weg oder einer Straße, der die Durchfahrt
   * verhindert.
   */
  BOLLARD,
  /**
   * Grenzkontrolle.
   */
  BORDER_CONTROL,
  /**
   * Ein sich selbst schließendes Tor - Kraftfahrer müssen nicht aussteigen um es zu öffnen oder
   * schließen. Meist in USA verwendet um einfach durch Viehzäune zu fahren.
   */
  BUMP_GATE,
  /**
   * See wikipedia.
   */
  BUS_TRAP,
  /**
   * Weiderost, "Bovi-Stop" (überfahrbarer Gitterrost, für Huftiere unüberwindbar).
   */
  CATTLE_GRID,
  /**
   * Kette zum Aufhalten von Kraftfahrzeugen.
   */
  CHAIN,
  /**
   * Beschränkung der Fahrraddurchfahrt. Gängige Bezeichnungen sind Drängelgitter oder Umlaufsperre.
   * Typischerweise zwei in den Weg hineinreichende versetzte Metallbügel, die das Durchfahren mit
   * Fahrrädern oder Motorrädern unmöglich machen oder zumindest verzögern, die Passage von
   * Fußgängern erlauben. In Deutschland oft am Ende von Radwegen um Radfahrer nicht direkt auf die
   * vorfahrtsberechtigte Straße fahren zu lassen oder das Befahren des Radweges mit motorisierten
   * Fahrzeugen zu verhindern. Sehr häufig finden sich solche Umlaufsperren auch an
   * Fußgängerüberwegen über Bahnstrecken.
   */
  CYCLE_BARRIER,
  /**
   * Eine Straße ist durch einen Erdwall abgesperrt. Wird oft als erste Maßnahme bei aufgegebenen
   * Straßen verwendet.
   */
  DEBRIS,
  /**
   * Eine Lücke in einer Barriere, nichts behindert den Durchgang. Standardmäßig gilt access=yes.
   * Mit den Rechten access=* eines durchführenden Weges kombinieren.
   */
  ENTRANCE,
  /**
   * A full-height turnstile, also called HEET-turnstile (high entrance/exit turnstile), like the
   * ones to access security areas. Note the mix of hyphen and underscore.
   */
  FULL_HEIGHT_TURNSTILE,
  /**
   * Tor, Schranke oder sonstige Absperrung, die geöffnet werden kann.
   */
  GATE,
  /**
   * Über den Weg gespannter Drahtzaun, der schnell entfernt werden kann.
   */
  HAMPSHIRE_GATE,
  /**
   * Combine with maxheight=*.
   */
  HEIGHT_RESTRICTOR,
  /**
   * Ein Zaunübertritt für Pferde ermöglicht es Fußgängern und Pferden eine Lücke im Zaun zu
   * überqueren, aber verhindert es nahezu, dass Motorräder und Vieh diese überqueren.
   */
  HORSE_STILE,
  /**
   * A Jersey barrier consists of heavy prefabricated blocks to create a barrier. Use
   * material=plastic or material=concrete to express the used material.
   */
  JERSEY_BARRIER,
  /**
   * A Kent carriage gap is used by local authorities in the UK to prevent motorised vehicles from
   * accessing public rights of way whilst allowing most horse drawn carriages to pass. These are
   * now becoming common on byways in the UK.
   */
  KENT_CARRIAGE_GAP,
  /**
   * Eine halb runde Stahlkonstruktion, die Menschen das passieren ermöglicht, Vieh aber abhält.
   */
  KISSING_GATE,
  /**
   * Schlagbaum.
   */
  LIFT_GATE,
  /**
   * Baum/Baumstamm (einer oder mehrere), i.d.R. querliegend.
   */
  LOG,
  /**
   * A flexible barrier made of fibres, twisted or braided together to improve strength. As a
   * barrier it is often more symbolic than actually physically preventing pedestrians from
   * accessing.
   */
  ROPE,
  /**
   * Ausfalltor, Durchgangsschleuse - Ein Durchgang mit einem inneren und einem äußeren Tor.
   */
  SALLY_PORT,
  /**
   * Spikes on the ground that prevent unauthorized access.
   */
  SPIKES,
  /**
   * Tritt über Zaun.
   */
  STILE,
  /**
   * A sump buster is a concrete slab or steel structure that prevents passing of two-tracked
   * vehicles with less than a minimum track and ground clearance. (Typically stops normal cars.)
   */
  SUMP_BUSTER,
  /**
   * Drehkreuz, lässt Personen einzeln ein- oder austreten, aber keine Tiere oder Fahrzeuge. Häufig
   * an Spielplätzen oder in Weidezäunen.
   */
  SWING_GATE,
  /**
   * Mautstelle.
   */
  TOLL_BOOTH,
}
