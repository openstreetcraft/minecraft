// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.bukkit.plugin.structure.block.Material;
import de.ixilon.osm.schema.OsmTag;
import de.ixilon.osm.schema.OsmWay;

public class FundamentRenderer extends PolygonRenderer {

  @Override
  protected boolean supports(Iterable<OsmTag> tags) {
    for (OsmTag tag : tags) {
      if ("building".equals(tag.getK())) {
        return true;
      }
    }
    return false;
  }

  @Override
  protected void render(final RenderImage image, final RenderModel model, OsmWay way) {
    model.traverse(location -> {
      if (isInside(model.getBoundingBox(location))) {
        long x = location.getX();
        long y = model.getHeight(location);
        long z = location.getZ();
        RenderImageUtil.removeVegetation(image, x, y + 1, z);
        image.setMaterial(x, y, z, Material.WHITE_CONCRETE);
      }
    }); 
  }
}
