// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;
import org.springframework.web.client.RestTemplate;

import de.ixilon.bukkit.plugin.structure.decoration.DecorationAreaRepository;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.CachingOverpassService;
import net.openstreetcraft.api.service.CoordinatesService;

public class StructurePlugin extends JavaPlugin implements Listener {
  private static int cacheSize = 9 * Bukkit.getMaxPlayers();
  private RestTemplate restTemplate;

  @Override
  public void onEnable() {
    RestTemplateFactory factory = new RestTemplateFactory(getClassLoader());
    restTemplate = factory.getRestTemplate();
    getServer().getPluginManager().registerEvents(this, this);
  }

  @Override
  public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
    return new CustomChunkGenerator(
        new DecorationAreaRepository(
            new CachingOverpassService(restTemplate, cacheSize),
            new CoordinatesService(restTemplate)));
  }

}
