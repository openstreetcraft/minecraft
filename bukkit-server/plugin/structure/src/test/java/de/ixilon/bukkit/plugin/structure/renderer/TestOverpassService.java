// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.springframework.web.client.RestTemplate;

import com.esri.core.geometry.Envelope2D;

import de.ixilon.osm.schema.ObjectFactory;
import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmAdapter;
import de.ixilon.osm.schema.OsmRoot;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import net.openstreetcraft.api.geom.RectangularBoundingBox;
import net.openstreetcraft.api.service.OverpassService;

public class TestOverpassService extends OverpassService {

  private final Osm osm;

  /**
   * Provides OSM data from XML file. 
   */
  public TestOverpassService(String filename) {
    super(new RestTemplate());
    try (InputStream input = TestOverpassService.class.getResourceAsStream(filename)) {
      osm = readOsm(input);
    } catch (JAXBException | XMLStreamException | IOException exception) {
      throw new IllegalArgumentException(filename, exception);
    }
  }

  /**
   * Provides OSM data from XML input stream. 
   */
  public TestOverpassService(InputStream input) {
    super(new RestTemplate());
    try {
      osm = readOsm(input);
    } catch (JAXBException | XMLStreamException exception) {
      throw new IllegalArgumentException(exception);
    }
  }

  private static Osm readOsm(InputStream input) throws JAXBException, XMLStreamException {
    JAXBContext context = JAXBContextFactory.createContext(new Class[] {ObjectFactory.class}, null);
    Unmarshaller unmarshaller = context.createUnmarshaller();
    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(input);
    @SuppressWarnings("unchecked")
    JAXBElement<OsmRoot> response = (JAXBElement<OsmRoot>) unmarshaller.unmarshal(xmlStreamReader);
    return new OsmAdapter(response.getValue());
  }

  @Override
  public Osm getOsm(RectangularBoundingBox bbox) {
    return osm;
  }

  @Override
  public Osm getOsm(Envelope2D bbox) {
    return osm;
  }
}
