// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import de.ixilon.bukkit.plugin.structure.decoration.DecorationArea;
import de.ixilon.bukkit.plugin.structure.decoration.DecorationAreaRepository;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.api.service.OverpassService;

public class BukkitTestRenderModelFactory implements RenderModelFactory {

  private final RenderModel renderModel;

  /**
   * Factory to create render models based on data read from a OSM XML file. 
   */
  public BukkitTestRenderModelFactory(String filename) {
    CoordinatesService coordinatesService = new TestCoordinatesService();
    OverpassService overpassService = new TestOverpassService(filename);
    DecorationArea decorationArea = new TestDecorationArea();
    DecorationAreaRepository repository =
        new DecorationAreaRepository(overpassService, coordinatesService, 1);
    renderModel = repository.getRenderModel(decorationArea);
  }

  @Override
  public RenderModel renderModel() {
    return renderModel;
  }
}
