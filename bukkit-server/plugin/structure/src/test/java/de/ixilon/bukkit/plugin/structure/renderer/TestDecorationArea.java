// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import org.bukkit.Material;

import de.ixilon.bukkit.plugin.structure.decoration.DecorationArea;

class TestDecorationArea extends DecorationArea {

  public TestDecorationArea() {
    super(null);
  }

  @Override
  public int getCenterX() {
    return 0;
  }

  @Override
  public int getCenterZ() {
    return 0;
  }

  public Material getBlock(int x, int y, int z) {
    if (y < 3) {
      return Material.BEDROCK;
    } else if (y < Math.min(x + z, 240)) {
      return Material.COBBLESTONE;
    }
    return Material.AIR;
  }

}
