// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import java.util.List;
import java.util.stream.Collectors;

import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.service.CoordinatesService;

public class TestCoordinatesService extends CoordinatesService {

  /**
   * Minecraft and spherical coordinates are identical.
   */
  public TestCoordinatesService() {
    super(null);
  }

  @Override
  public List<SphericalLocation> getSphericalLocations(List<MinecraftLocation> locations,
      World world) {
    return locations.stream().map(loc -> getSphericalLocation(loc, null))
        .collect(Collectors.toList());
  }

  @Override
  public SphericalLocation getSphericalLocation(MinecraftLocation location, World world) {
    return new SphericalLocation(-location.getZ(), location.getX());
  }

  @Override
  public List<MinecraftLocation> getMinecraftLocations(List<SphericalLocation> locations,
      World world) {
    return locations.stream().map(loc -> getMinecraftLocation(loc, null))
        .collect(Collectors.toList());
  }

  @Override
  public MinecraftLocation getMinecraftLocation(SphericalLocation location, World world) {
    return new MinecraftLocation(toLong(location.getLongitude()), toLong(-location.getLatitude()));
  }

  private static long toLong(double value) {
    return Double.valueOf(value).longValue();
  }

}
