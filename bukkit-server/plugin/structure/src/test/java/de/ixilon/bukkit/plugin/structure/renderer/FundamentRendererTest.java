// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import org.junit.Ignore;
import org.junit.Test;

public class FundamentRendererTest {

  private final BukkitTestRenderModelFactory factory =
      new BukkitTestRenderModelFactory("fundament.xml");
  private final FundamentRendererTestSupport support = new FundamentRendererTestSupport();
  private final FundamentRenderer renderer = new FundamentRenderer();

  @Ignore
  @Test
  public void rendersFundament() {
    support.verifyFundamentRenderer(renderer, factory.renderModel());
  }

}
