// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.structure.renderer;

import org.mockito.Mockito;

import de.ixilon.bukkit.plugin.structure.block.Material;
import de.ixilon.bukkit.plugin.structure.block.UnknownMaterialException;

public class FundamentRendererTestSupport {

  /**
   * Verify if fundament is rendered correctly. 
   */
  public void verifyFundamentRenderer(Renderer renderer, RenderModel model) {
    RenderImage image = Mockito.mock(RenderImage.class);
    
    try {
      Mockito.when(image.getMaterial(Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong()))
          .thenReturn(Material.AIR);
    } catch (UnknownMaterialException exception) {
      // ignore
    }

    renderer.render(image, model);

    Mockito.verify(image).setMaterial(2, 2, -8, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(3, 2, -8, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(4, 2, -8, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(5, 2, -8, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(6, 2, -8, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(7, 2, -8, Material.WHITE_CONCRETE);
    
    Mockito.verify(image).setMaterial(2, 2, -7, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(3, 2, -7, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(4, 2, -7, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(5, 2, -7, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(6, 2, -7, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(7, 2, -7, Material.WHITE_CONCRETE);
    
    Mockito.verify(image).setMaterial(2, 2, -6, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(3, 2, -6, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(4, 2, -6, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(5, 2, -6, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(6, 2, -6, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(7, 2, -6, Material.WHITE_CONCRETE);
    
    Mockito.verify(image).setMaterial(2, 2, -5, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(3, 2, -5, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(4, 2, -5, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(5, 2, -5, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(6, 2, -5, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(7, 2, -5, Material.WHITE_CONCRETE);
    
    Mockito.verify(image).setMaterial(2, 2, -4, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(3, 2, -4, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(4, 2, -4, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(5, 2, -4, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(6, 2, -4, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(7, 2, -4, Material.WHITE_CONCRETE);
    
    Mockito.verify(image).setMaterial(2, 2, -3, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(3, 2, -3, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(4, 2, -3, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(5, 2, -3, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(6, 2, -3, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(7, 3, -3, Material.WHITE_CONCRETE);
    
    Mockito.verify(image).setMaterial(2, 2, -2, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(3, 2, -2, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(4, 2, -2, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(5, 2, -2, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(6, 3, -2, Material.WHITE_CONCRETE);
    Mockito.verify(image).setMaterial(7, 4, -2, Material.WHITE_CONCRETE);

    try {
      Mockito.verify(image, Mockito.atLeast(0)).getMaterial(Mockito.anyLong(), Mockito.anyLong(),
          Mockito.anyLong());
    } catch (UnknownMaterialException exception) {
      // ignore
    }

    Mockito.verifyNoMoreInteractions(image);
  }
}
