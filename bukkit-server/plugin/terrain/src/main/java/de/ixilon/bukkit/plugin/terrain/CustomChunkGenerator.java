// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.terrain;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import de.ixilon.minecraft.bukkit.BukkitBiomeConverter;
import de.ixilon.minecraft.plugin.biome.Biomes;
import de.ixilon.minecraft.plugin.terrain.Terrain;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.minecraft.biome.Biome;

public class CustomChunkGenerator extends ChunkGenerator {

  private static final int CHUNK_SIZE = 16;
  private static final BukkitBiomeConverter CONVERTER = new BukkitBiomeConverter();
  private static final net.openstreetcraft.api.model.World WORLD =
      net.openstreetcraft.api.model.World.EARTH;
  
  private final Terrain terrain;

  public CustomChunkGenerator(Terrain terrain) {
    this.terrain = terrain;
  }

  @Override
  public ChunkData generateChunkData(World world, Random random, int chunkX, int chunkZ,
      BiomeGrid biomeGrid) {
    ChunkData chunk = createChunkData(world);

    int xmin = chunkX * CHUNK_SIZE;
    int zmin = chunkZ * CHUNK_SIZE;
    int xmax = xmin + CHUNK_SIZE;
    int zmax = zmin + CHUNK_SIZE;

    chunk.setRegion(xmin, 0, zmin, xmax, 1, zmax, Material.BEDROCK);

    for (int dz = 0; dz < CHUNK_SIZE; dz++) {
      for (int dx = 0; dx < CHUNK_SIZE; dx++) {
        MinecraftLocation location = new MinecraftLocation(xmin + dx, zmin + dz);
        int height = terrain.surface(WORLD, location);
        
        height = Math.max(height, 2);
        height = Math.min(height, chunk.getMaxHeight());

        chunk.setRegion(dx, 1, dz, dx, height, dz, Material.STONE);
        
        Biome biome = terrain.biome(WORLD, location);

        if (Biomes.isOcean(biome)) {
          chunk.setRegion(dx, height, dz, dx, world.getSeaLevel(), dz, Material.WATER);
        } else if (Biomes.isWater(biome)) {
          chunk.setBlock(dx, height - 1, dz, Material.WATER);
        }
        
        biomeGrid.setBiome(dx, dz, CONVERTER.apply(biome));
      }
    }

    return chunk;
  }
}
