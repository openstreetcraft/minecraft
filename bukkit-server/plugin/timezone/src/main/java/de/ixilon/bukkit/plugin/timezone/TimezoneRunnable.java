// Copyright (C) 2018 Wout Ceulemans <wout.ceulemans@student.kuleuven.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.bukkit.plugin.timezone;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import org.springframework.web.client.RestTemplate;

import de.ixilon.minecraft.plugin.timezone.TimezoneUtils;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.Timezone;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.api.service.TimezoneService;

public class TimezoneRunnable implements Runnable {
  private final Server server;
  private final World world;
  private final CoordinatesService coordinatesService;
  private final TimezoneService timezoneService;

  /**
   * Constructs a new TimezoneRunnable for a bukkit server.
   * @param server not-null server instance for which time TimezoneRunnable runs
   */
  public TimezoneRunnable(Server server) {
    this.server = server;
    this.world = World.of("earth");

    RestTemplateFactory templateFactory = new RestTemplateFactory(
        server.getClass().getClassLoader());
    RestTemplate restTemplate = templateFactory.getRestTemplate();

    this.coordinatesService = new CoordinatesService(restTemplate);
    this.timezoneService = new TimezoneService(restTemplate);
  }

  @Override
  public void run() {
    Set<Chunk> chunks = server.getOnlinePlayers().stream()
        .map(player -> player.getLocation().getChunk())
        .collect(Collectors.toSet());

    for (Chunk chunk : chunks) {
      Location location = chunk.getBlock(7, 0, 7).getLocation();

      SphericalLocation sphericalLocation;
      Timezone timezone;
      try {
        sphericalLocation = coordinatesService.getSphericalLocation(
            new MinecraftLocation(location.getBlockX(), location.getBlockZ()), world);

        timezone = timezoneService.getTimezone(sphericalLocation);
      } catch (Exception ignored) {
        continue;
      }

      int time = TimezoneUtils.toTicks(timezone.getCurrentTime());

      Set<Player> players = Stream.of(chunk.getEntities())
          .filter(e -> e instanceof Player)
          .map(e -> (Player) e)
          .collect(Collectors.toSet());

      for (Player player : players) {
        player.setPlayerTime(time, false);
      }
    }
  }
}
