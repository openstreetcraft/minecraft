// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit.graphics;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.awt.Color;
import java.awt.Graphics2D;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import net.openstreetcraft.graphics.Colormap;

public class BukkitImageTest {

  private static final int LEVEL = 42;

  @Mock
  private Chunk chunk;

  @Mock
  private Block block;

  @Mock
  private BlockState blockState;

  private BukkitImage image;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    when(block.getState()).thenReturn(blockState);
    when(chunk.getBlock(anyInt(), eq(LEVEL), anyInt())).thenReturn(block);
    when(chunk.getX()).thenReturn(1);
    when(chunk.getZ()).thenReturn(2);

    image = new BukkitImage(chunk, LEVEL);
  }

  @Test
  public void drawsLine() {
    Graphics2D graphics = image.createGraphics();
    Colormap<Material> colormap = image.getColormap();
    Color color = colormap.getColor(Material.BEDROCK);

    graphics.setColor(color);
    graphics.drawLine(3, 10, 7, 17);
    graphics.dispose();

    verify(chunk).getBlock(3, LEVEL, 10);
    verify(chunk).getBlock(4, LEVEL, 11);
    verify(chunk).getBlock(4, LEVEL, 12);
    verify(chunk).getBlock(5, LEVEL, 13);
    verify(chunk).getBlock(5, LEVEL, 14);
    verify(chunk).getBlock(6, LEVEL, 15);

    // clipped pixels
    // verify(chunk).getBlock(6, LEVEL, 16);
    // verify(chunk).getBlock(7, LEVEL, 17);

    verify(block, times(6)).setType(Material.BEDROCK);
  }

}
