// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit.graphics;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;

import net.openstreetcraft.graphics.Canvas;

public class BukkitCanvas implements Canvas<Material> {
  private final Chunk chunk;
  private final int level;
  
  public BukkitCanvas(Chunk chunk, int level) {
    this.chunk = chunk;
    this.level = level;
  }

  @Override
  public int getWidth() {
    return 16;
  }

  @Override
  public int getHeight() {
    return 16;
  }

  @Override
  public void setItem(int xpos, int ypos, Material item) {
    Block block = chunk.getBlock(xpos, level, ypos);
    block.setType(item);
    block.getState().update();
  }

  @Override
  public Material getItem(int xpos, int ypos) {
    Block block = chunk.getBlock(xpos, level, ypos);
    return block.getType();
  }
}
