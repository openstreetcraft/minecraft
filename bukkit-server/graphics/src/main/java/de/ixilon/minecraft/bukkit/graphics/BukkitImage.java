// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit.graphics;

import org.bukkit.Chunk;
import org.bukkit.Material;

import net.openstreetcraft.graphics.Colormap;
import net.openstreetcraft.graphics.Image;

public class BukkitImage extends Image<Material> {
  /**
   * Draws Minecraft blocks into the chunk using Java 2D. Drawing operations appear as blocks on a
   * slice of the chunk at the given height level. The north-west block in the chunk has pixel
   * coordinate (0, 0). Block material must be mapped to colors with
   * <pre>
   * Colormap&lt;Material&gt; colormap = image.getColormap();
   * Color color = colormap.getColor(Material.BEDROCK);
   * </pre>
   * Graphics context can be created with:
   * <pre>
   * Graphics2D graphics = image.createGraphics();
   * </pre>
   */
  public BukkitImage(Chunk chunk, int level) {
    super(new BukkitCanvas(chunk, level), new Colormap<Material>());
  }
}
