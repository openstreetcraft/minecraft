# Bukkit Graphics

Library for drawing Minecraft blocks with Java 2D on a Minecraft map using Graphics2D context.

Based on the [graphics library](https://gitlab.com/openstreetcraft/minecraft/lib/graphics).
