// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import de.ixilon.minecraft.plugin.biome.BiomeService;
import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.geom.RectangularBoundingBox;

public class BukkitBiomeService {
  private final BukkitBiomeConverter converter = new BukkitBiomeConverter();
  private final BiomeService service;

  public BukkitBiomeService(RestTemplate restTemplate) {
    this.service = new BiomeService(restTemplate);
  }

  /**
   * Returns width * height biomes within a given bounding box of spherical coordinates.
   */
  public Raster<org.bukkit.block.Biome> biomes(RectangularBoundingBox bbox, int width, int height)
      throws RestClientException {
    return service.biomes(bbox, width, height).convert(converter);
  }
}
