// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.ixilon.minecraft.MinecraftPlayer;
import net.openstreetcraft.api.model.MinecraftLocation;

/**
 * This abstract factory creates an abstract player of a concrete Bukkit player instance. 
 */
public class BukkitMinecraftPlayer implements MinecraftPlayer {
  
  private final Player player;
  
  private BukkitMinecraftPlayer(JavaPlugin plugin, Player player) {
    this.player = player;
  }

  public static MinecraftPlayer of(JavaPlugin plugin, Player player) {
    return new BukkitMinecraftPlayer(plugin, player);
  }

  @Override
  public void sendMessage(String message) {
    player.sendMessage(message);
  }

  @Override
  public MinecraftLocation getMinecraftLocation() {
    Location location = player.getLocation();
    return new MinecraftLocation(location.getBlockX(), location.getBlockZ());
  }

  @Override
  public void setMinecraftLocation(MinecraftLocation minecraftLocation) {
    Location location = player.getLocation();
    
    location.setX(minecraftLocation.getX());
    location.setZ(minecraftLocation.getZ());
    location.setY(player.getWorld().getHighestBlockYAt(location) + 1);

    player.teleport(location);
  }

}
