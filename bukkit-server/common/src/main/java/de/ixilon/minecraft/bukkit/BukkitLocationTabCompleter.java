// Copyright (C) 2021 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.ImmutableList;

import de.ixilon.minecraft.MinecraftPlayer;
import de.ixilon.minecraft.plugin.location.LocationTabCompleter;

public class BukkitLocationTabCompleter extends LocationTabCompleter implements TabCompleter {

  private final JavaPlugin plugin;
  private final PluginCommand command;

  /**
   * Bukkit variant of a location tab completer.
   */
  public BukkitLocationTabCompleter(JavaPlugin plugin, ClassLoader classLoader,
      PluginCommand command) {
    super(classLoader);
    this.plugin = plugin;
    this.command = command;
  }

  @Override
  public ImmutableList<String> onTabComplete(CommandSender sender, Command command, String alias,
      String[] args) {
    if (!command.equals(this.command) || !(sender instanceof Player)) {
      return null;
    }
    MinecraftPlayer player = BukkitMinecraftPlayer.of(plugin, (Player) sender);
    return super.onTabComplete(player, args);
  }
}
