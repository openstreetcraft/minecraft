// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit;

import org.bukkit.plugin.java.JavaPlugin;

import de.ixilon.minecraft.plugin.terrain.Terrain;
import de.ixilon.minecraft.plugin.terrain.TerrainImpl;

public class BukkitTerrain {
  /**
   * Return Terrain API of openstreetcraft-terrain plugin.
   * @throws IllegalArgumentException if plugin is not loaded.
   */
  public static Terrain getInstance() throws IllegalArgumentException {
    return (Terrain) JavaPlugin.getProvidingPlugin(TerrainImpl.class);
  }
}
