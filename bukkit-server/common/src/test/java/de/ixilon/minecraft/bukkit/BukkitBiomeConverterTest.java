// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.bukkit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BukkitBiomeConverterTest {
  private final BukkitBiomeConverter converter = new BukkitBiomeConverter();

  @Test
  public void convertsToBukkitBiomes() {
    assertEquals(org.bukkit.block.Biome.BAMBOO_JUNGLE,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.BAMBOO_JUNGLE));
//    assertEquals(org.bukkit.block.Biome.BAMBOO_JUNGLE_HILLS,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.BAMBOO_JUNGLE_HILLS));
//    assertEquals(org.bukkit.block.Biome.BASALT_DELTAS,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.BASALT_DELTAS));
    assertEquals(org.bukkit.block.Biome.BEACH,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.BEACH));
    assertEquals(org.bukkit.block.Biome.BIRCH_FOREST,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.BIRCH_FOREST));
//    assertEquals(org.bukkit.block.Biome.BIRCH_FOREST_HILLS,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.BIRCH_FOREST_HILLS));
    assertEquals(org.bukkit.block.Biome.COLD_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.COLD_OCEAN));
//    assertEquals(org.bukkit.block.Biome.CRIMSON_FOREST,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.CRIMSON_FOREST));
    assertEquals(org.bukkit.block.Biome.DEEP_COLD_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.DEEP_COLD_OCEAN));
    assertEquals(org.bukkit.block.Biome.DEEP_FROZEN_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.DEEP_FROZEN_OCEAN));
    assertEquals(org.bukkit.block.Biome.DEEP_LUKEWARM_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.DEEP_LUKEWARM_OCEAN));
    assertEquals(org.bukkit.block.Biome.DEEP_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.DEEP_OCEAN));
//    assertEquals(org.bukkit.block.Biome.DEEP_WARM_OCEAN,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.DEEP_WARM_OCEAN));
    assertEquals(org.bukkit.block.Biome.DESERT,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.DESERT));
//    assertEquals(org.bukkit.block.Biome.DESERT_HILLS,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.DESERT_HILLS));
    assertEquals(org.bukkit.block.Biome.FLOWER_FOREST,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.FLOWER_FOREST));
    assertEquals(org.bukkit.block.Biome.FOREST,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.FOREST));
    assertEquals(org.bukkit.block.Biome.FROZEN_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.FROZEN_OCEAN));
    assertEquals(org.bukkit.block.Biome.FROZEN_RIVER,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.FROZEN_RIVER));
    assertEquals(org.bukkit.block.Biome.JUNGLE,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.JUNGLE));
//    assertEquals(org.bukkit.block.Biome.JUNGLE_EDGE,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.JUNGLE_EDGE));
//    assertEquals(org.bukkit.block.Biome.JUNGLE_HILLS,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.JUNGLE_HILLS));
    assertEquals(org.bukkit.block.Biome.LUKEWARM_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.LUKEWARM_OCEAN));
    assertEquals(org.bukkit.block.Biome.OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.OCEAN));
    assertEquals(org.bukkit.block.Biome.PLAINS,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.PLAINS));
    assertEquals(org.bukkit.block.Biome.RIVER,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.RIVER));
    assertEquals(org.bukkit.block.Biome.SAVANNA,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.SAVANNA));
    assertEquals(org.bukkit.block.Biome.SAVANNA_PLATEAU,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.SAVANNA_PLATEAU));
//    assertEquals(org.bukkit.block.Biome.SOUL_SAND_VALLEY,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.SOUL_SAND_VALLEY));
    assertEquals(org.bukkit.block.Biome.SUNFLOWER_PLAINS,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.SUNFLOWER_PLAINS));
    assertEquals(org.bukkit.block.Biome.SWAMP,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.SWAMP));
    assertEquals(org.bukkit.block.Biome.TAIGA,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.TAIGA));
//    assertEquals(org.bukkit.block.Biome.TAIGA_HILLS,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.TAIGA_HILLS));
    assertEquals(org.bukkit.block.Biome.THE_END,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.THE_END));
    assertEquals(org.bukkit.block.Biome.WARM_OCEAN,
        converter.apply(net.openstreetcraft.minecraft.biome.Biome.WARM_OCEAN));
//    assertEquals(org.bukkit.block.Biome.WARPED_FOREST,
//        converter.apply(net.openstreetcraft.minecraft.biome.Biome.WARPED_FOREST));
  }

}
