// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.nukkit;

import static java.lang.Math.toIntExact;

import cn.nukkit.Player;
import cn.nukkit.level.Location;
import cn.nukkit.level.Position;
import de.ixilon.minecraft.MinecraftPlayer;
import net.openstreetcraft.api.model.MinecraftLocation;

/**
 * This abstract factory creates an abstract player of a concrete Nukkit player instance.
 */
public class NukkitMinecraftPlayer implements MinecraftPlayer {

  private final Player player;

  private NukkitMinecraftPlayer(Player player) {
    this.player = player;
  }

  public static MinecraftPlayer of(Player player) {
    return new NukkitMinecraftPlayer(player);
  }

  @Override
  public void sendMessage(String message) {
    player.sendMessage(message);
  }

  @Override
  public MinecraftLocation getMinecraftLocation() {
    Location location = player.getLocation();
    return new MinecraftLocation(location.getFloorX(), location.getFloorZ());
  }

  @Override
  public void setMinecraftLocation(MinecraftLocation minecraftLocation) {
    final long xpos = minecraftLocation.getX();
    final long zpos = minecraftLocation.getZ();

    // player will fall out of the world, but it also forces the chunk to load
    player.teleport(new Position(xpos, -5, zpos));

    player.getServer().getScheduler().scheduleDelayedTask(new Runnable() {
      @Override
      public void run() {
        long ypos = player.getLevel().getHighestBlockAt(toIntExact(xpos), toIntExact(zpos));
        player.teleport(new Position(xpos, ypos + 1, zpos));
      }
    }, 25);
  }

}
