// Copyright (C) 2017 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.nukkit;

import cn.nukkit.Server;
import de.ixilon.instrument.PatchAgentLoader;

public class Nukkit {

  /**
   * Nukkit server main program.
   */
  public static void main(String[] args) {
    // Use Java standard logging framework
    PatchAgentLoader.loadAgent();

    // Gracefull shutdown on kill signal
    Runtime.getRuntime().addShutdownHook(new Thread() {
      public void run() {
        Server.getInstance().forceShutdown();
      }
    });

    String[] newargs = {"disable-ansi"};
    cn.nukkit.Nukkit.main(newargs);
  }

}
