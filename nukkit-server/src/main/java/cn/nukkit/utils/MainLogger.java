// Copyright (C) 2017 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cn.nukkit.utils;

import java.util.logging.Level;

public class MainLogger extends ThreadedLogger {

  public static MainLogger getLogger() {
    return new MainLogger("");
  }

  public MainLogger(String logFile) {
    this(logFile, false);
  }

  public MainLogger(String logFile, Boolean logDebug) {}

  @Override
  public void emergency(String message) {
    logger().log(Level.SEVERE, message);
  }

  @Override
  public void emergency(String message, Throwable throwable) {
    logger().log(Level.SEVERE, message, throwable);
  }

  @Override
  public void alert(String message) {
    logger().log(Level.SEVERE, message);
  }

  @Override
  public void alert(String message, Throwable throwable) {
    logger().log(Level.SEVERE, message, throwable);
  }

  @Override
  public void critical(String message) {
    logger().log(Level.SEVERE, message);
  }

  @Override
  public void critical(String message, Throwable throwable) {
    logger().log(Level.SEVERE, message, throwable);
  }

  @Override
  public void error(String message) {
    logger().log(Level.SEVERE, message);
  }

  @Override
  public void error(String message, Throwable throwable) {
    logger().log(Level.SEVERE, message, throwable);
  }

  @Override
  public void warning(String message) {
    logger().log(Level.WARNING, message);
  }

  @Override
  public void warning(String message, Throwable throwable) {
    logger().log(Level.WARNING, message, throwable);
  }

  @Override
  public void notice(String message) {
    logger().log(Level.INFO, message);
  }

  @Override
  public void notice(String message, Throwable throwable) {
    logger().log(Level.INFO, message, throwable);
  }

  @Override
  public void info(String message) {
    logger().log(Level.INFO, message);
  }

  @Override
  public void info(String message, Throwable throwable) {
    logger().log(Level.INFO, message, throwable);
  }

  @Override
  public void debug(String message) {
    logger().log(Level.FINE, message);
  }

  @Override
  public void debug(String message, Throwable throwable) {
    logger().log(Level.FINE, message, throwable);
  }

  private java.util.logging.Logger logger() {
    return java.util.logging.Logger
        .getLogger(CallingClass.INSTANCE.getCallingClasses()[3].getName());
  }

  @Override
  public void log(LogLevel level, String message) {
    switch (level) {
      case EMERGENCY:
      case ALERT:
      case CRITICAL:
      case ERROR:
        error(message);
        break;
      case WARNING:
        warning(message);
        break;
      case NOTICE:
      case INFO:
        info(message);
        break;
      case DEBUG:
        debug(message);
        break;
      case NONE:
        break;
      default:
        break;
    }
  }

  @Override
  public void log(LogLevel level, String message, Throwable throwable) {
    switch (level) {
      case EMERGENCY:
      case ALERT:
      case CRITICAL:
      case ERROR:
        error(message, throwable);
        break;
      case WARNING:
        warning(message, throwable);
        break;
      case NOTICE:
      case INFO:
        info(message, throwable);
        break;
      case DEBUG:
        debug(message, throwable);
        break;
      case NONE:
        break;
      default:
        break;
    }
  }

  public void setLogDebug(Boolean logDebug) {}

  public void logException(Exception exception) {
    alert(Utils.getExceptionMessage(exception));
  }

  @Override
  public void run() {}

  public void shutdown() {}

  private static class CallingClass extends SecurityManager {
    static final CallingClass INSTANCE = new CallingClass();

    Class[] getCallingClasses() {
      return getClassContext();
    }
  }

}
