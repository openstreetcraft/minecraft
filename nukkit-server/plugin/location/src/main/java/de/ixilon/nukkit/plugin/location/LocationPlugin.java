// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.nukkit.plugin.location;

import cn.nukkit.Player;
import cn.nukkit.command.Command;
import cn.nukkit.command.CommandSender;
import cn.nukkit.plugin.PluginBase;
import de.ixilon.minecraft.nukkit.NukkitMinecraftPlayer;
import de.ixilon.minecraft.plugin.location.LocationCommandExecutor;

public class LocationPlugin extends PluginBase {

  private LocationCommandExecutor executor;

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    switch (command.getName().toLowerCase()) {
      case "location":
        if (sender instanceof Player) {
          executor.onCommand(NukkitMinecraftPlayer.of((Player) sender), args);
          return true;
        }
        break;
      default:
        return false;
    }
    return false;
  }

  @Override
  public void onEnable() {
    executor = new LocationCommandExecutor(getClass().getClassLoader());
    getServer().getPluginManager().registerEvents(new LocationListener(), this);
  }
}
