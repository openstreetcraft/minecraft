// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.nukkit.plugin.location;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.Listener;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerRespawnEvent;
import cn.nukkit.event.player.PlayerTeleportEvent;
import cn.nukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class LocationListener implements Listener {

  /**
   * Called when player joins the server.
   */
  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    performCommand(event.getPlayer());
  }

  /**
   * Called when player respawns.
   */
  @EventHandler
  public void onPlayerRespawn(PlayerRespawnEvent event) {
    performCommand(event.getPlayer());
  }

  /**
   * Called when player teleports to a new location.
   */
  @EventHandler
  public void onPlayerTeleport(PlayerTeleportEvent event) {
    if (event.getCause() == TeleportCause.PLUGIN) {
      performCommand(event.getPlayer());
    }
  }

  private void performCommand(final Player player) {
    player.getServer().getCommandMap().dispatch(player, "location");
  }

}
