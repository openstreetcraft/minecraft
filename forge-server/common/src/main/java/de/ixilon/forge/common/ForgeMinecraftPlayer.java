// Copyright (C) 2020 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


package de.ixilon.forge.common;

import de.ixilon.minecraft.MinecraftPlayer;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.openstreetcraft.api.model.MinecraftLocation;

public class ForgeMinecraftPlayer implements MinecraftPlayer {
  
  private final EntityPlayerMP player;
  
  private ForgeMinecraftPlayer(EntityPlayerMP player) {
    this.player = player;
  }
  
  /**
   * Creates a generic player object of a Forge specific player object.
   */
  public static MinecraftPlayer of(EntityPlayerMP player) {
    return new ForgeMinecraftPlayer(player);
  }
  
  /**
   * Sends a message to the player.
   */
  @Override
  public void sendMessage(String message) {
    TextComponentString text = new TextComponentString(message);
    player.sendMessage(text);
  }
  
  /**
   * Returns the x and z of the player.
   */
  @Override
  public MinecraftLocation getMinecraftLocation() {
    BlockPos location = player.getPosition();
    return new MinecraftLocation(location.getX(), location.getZ());
  }
  
  /**
   * Teleports to the highest block of the x and z coordinates.
   */
  @Override
  public void setMinecraftLocation(MinecraftLocation location) {
    double highestBlock = (double) player.getServerWorld()
        .getHeight((int) location.getX(), (int) location.getZ());
      
    player.setPositionAndUpdate((double) location.getX(), highestBlock, (double) location.getZ());
  }

}
