# Forge Server
This project brings the Openstreetcraft project to [Minecraft Forge toolset](https://forums.minecraftforge.net/). Minecraft Forge is known as the modding toolset for Minecraft, it is very extendable and has countless features.

### Mods
We are going to ported most of the plugins from bukkit and alike to Forge. We currently have integrated the following mods:
* Location
* Sample
* CubicChunks (to work with the [Build the Earth](https://buildtheearth.net/) project)

### How to run
To start the forge server you'll need to install `docker-compose` instructions be [found here](https://docs.docker.com/compose/install/). Once `docker-compose` is installed you can simply clone the repository, navigate to `forge-server` and run the command:

```
docker-compose up
```

## Developer Information

### Common Library
The common library is used to allow the [Openstreetcraft Minecraft common library] (https://gitlab.com/openstreetcraft/minecraft/-/tree/master/lib/common) to communicate with the forge framework.