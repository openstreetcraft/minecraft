// Copyright (C) 2020 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.forge.mod.sample;

import org.apache.logging.log4j.Logger;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = SampleMod.MODID, name = SampleMod.NAME, version = SampleMod.VERSION)
public class SampleMod {
  public static final String MODID = "openstreetcraftsamplemod";
  public static final String NAME = "Openstreetcraft Sample Mod";
  public static final String VERSION = "1.0.0";

  private Logger logger;

  @EventHandler
  public void preInit(FMLPreInitializationEvent event) {
    logger = event.getModLog();
    logger.info("Pre Initialization Event");
  }

  @EventHandler
  public void init(FMLInitializationEvent event) {
    logger.info("Initialization Event");
  }

  @EventHandler
  public void postInit(FMLPostInitializationEvent event) {
    logger.info("Post Initialization Event");
  }
}
