// Copyright (C) 2020 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.forge.mod.location;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid = LocationMod.MODID, name = LocationMod.NAME, version = LocationMod.VERSION,
    serverSideOnly = true, acceptableRemoteVersions = "*")
public class LocationMod {
  public static final String MODID = "openstreetcraftlocationmod";
  public static final String NAME = "Openstreetcraft Location Mod";
  public static final String VERSION = "1.0.0";
  
  /**
   * Initialisation event.
   * Registers event listener.
   */
  @EventHandler
  public void init(FMLInitializationEvent event) {
    LocationListener listener = new LocationListener();
    MinecraftForge.EVENT_BUS.register(listener);
  }
  
  /**
   * Server starting event.
   * Registers location command.
   */
  @EventHandler
  public void serverStarting(FMLServerStartingEvent event) {
    event.registerServerCommand(new ForgeLocationCommandExecutor(getClass().getClassLoader()));
  }
}
