// Copyright (C) 2020 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.forge.mod.location;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;

public class LocationListener {
  
  private final ForgeLocationCommandExecutor executor = new ForgeLocationCommandExecutor(
      getClass().getClassLoader());
  
  /**
   * Called when the player joins the server.
   */
  @SubscribeEvent
  public void onPlayerJoin(PlayerLoggedInEvent event) throws CommandException {
    performCommand(event.player);
  }
  
  /**
   * Called when the player respawns.
   */
  @SubscribeEvent
  public void onPlayerRespawn(PlayerRespawnEvent event) throws CommandException {
    performCommand(event.player);
  }
  
  // TODO Implement Teleport Event
  
  /**
   * Executes the 'location' command, with no arguments.
   */
  private void performCommand(final EntityPlayer player) throws CommandException {
    String[] args = {};
    executor.execute(player.getServer(), (ICommandSender) player, args);
  }
}
