// Copyright (C) 2020 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.forge.mod.location;

import de.ixilon.forge.common.ForgeMinecraftPlayer;
import de.ixilon.minecraft.plugin.location.LocationCommandExecutor;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

public class ForgeLocationCommandExecutor extends CommandBase {
  
  private final LocationCommandExecutor executor;
  
  public ForgeLocationCommandExecutor(ClassLoader classLoader) {
    executor = new LocationCommandExecutor(classLoader);
  }
  
  /**
   * Returns the name of the command.
   */
  @Override
  public String getName() {
    return "location";
  }
  
  /**
   * Returns the usage of the command.
   */
  @Override
  public String getUsage(ICommandSender sender) {
    return "/location <lat> <lon> | <address>";
  }
  
  /**
   * Called when the command is executed.
   * Forwards the command sender and arguments to 
   * LocationCommandExecutor to do the necessary operations.
   */
  @Override
  public void execute(MinecraftServer server, ICommandSender sender, String[] args)
      throws CommandException {
    executor.onCommand(ForgeMinecraftPlayer.of((EntityPlayerMP) sender), args);
  }
  
}
