// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.graphics;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Colormap<T> {
  private final List<T> colormap = new ArrayList<>();

  /**
   * Maps an item to an unique AWT color. Subsequent calls with the same item will result in the
   * same color.
   */
  public synchronized Color getColor(T item) {
    if (!colormap.contains(item)) {
      colormap.add(item);
    }
    int rgb = colormap.indexOf(item);
    return new Color(rgb);
  }

  /**
   * The item associated with a given AWT color. Item must have been registered with getColor().
   */
  public synchronized T getItem(Color color) {
    int rgb = color.getRGB() & 0xFFFFFF;
    return colormap.get(rgb);
  }
}
