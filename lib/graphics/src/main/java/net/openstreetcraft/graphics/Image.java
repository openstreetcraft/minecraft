// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.graphics;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DirectColorModel;
import java.awt.image.SampleModel;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;

public class Image<T> extends BufferedImage {
  private static final int MASK_A = 0xff000000;
  private static final int MASK_R = 0x00ff0000;
  private static final int MASK_G = 0x0000ff00;
  private static final int MASK_B = 0x000000ff;

  private final Canvas<T> canvas;
  private final Colormap<T> colormap;

  /**
   * Buffered image backed by an abstract canvas.
   */
  public Image(Canvas<T> canvas, Colormap<T> colormap) {
    super(createColorModel(), createRaster(canvas, colormap), false, null);
    this.canvas = canvas;
    this.colormap = colormap;
  }

  public Canvas<T> getCanvas() {
    return canvas;
  }

  public Colormap<T> getColormap() {
    return colormap;
  }

  private static ColorModel createColorModel() {
    return new DirectColorModel(32, MASK_R, MASK_G, MASK_B, MASK_A);
  }

  private static <T> WritableRaster createRaster(Canvas<T> canvas, Colormap<T> colormap) {
    SampleModel sampleModel = createSampleModel(canvas.getWidth(), canvas.getHeight());
    DataBuffer dataBuffer = createDataBuffer(canvas, colormap);
    return new CanvasRaster<T>(sampleModel, dataBuffer);
  }

  private static SampleModel createSampleModel(int width, int height) {
    int[] masks = new int[] {MASK_R, MASK_G, MASK_B, MASK_A};
    return new SinglePixelPackedSampleModel(DataBuffer.TYPE_INT, width, height, masks);
  }

  private static <T> DataBuffer createDataBuffer(Canvas<T> canvas, Colormap<T> colormap) {
    return new CanvasDataBuffer<T>(canvas, colormap);
  }

  private static class CanvasRaster<T> extends WritableRaster {
    public CanvasRaster(SampleModel sampleModel, DataBuffer dataBuffer) {
      super(sampleModel, dataBuffer, new Point(0, 0));
    }
  }

  private static class CanvasDataBuffer<T> extends DataBuffer {
    private final Canvas<T> canvas;
    private final Colormap<T> colormap;

    public CanvasDataBuffer(Canvas<T> canvas, Colormap<T> colormap) {
      super(TYPE_INT, canvas.getWidth() * canvas.getHeight());
      this.canvas = canvas;
      this.colormap = colormap;
    }

    @Override
    public int getElem(int bank, int index) {
      int width = canvas.getWidth();
      int xpos = index % width;
      int ypos = index / width;
      T item = canvas.getItem(xpos, ypos);
      Color color = colormap.getColor(item);
      return color.getRGB();
    }

    @Override
    public void setElem(int bank, int index, int value) {
      int width = canvas.getWidth();
      int xpos = index % width;
      int ypos = index / width;
      Color color = new Color(value);
      T item = colormap.getItem(color);
      canvas.setItem(xpos, ypos, item);
    }
  }
}
