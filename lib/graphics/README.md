# Graphics

Library for drawing arbitrary objects with Java 2D on an abstract canvas using Graphics2D context.

Graphics2D is the fundamental class for rendering 2-dimensional shapes, text and images on the Java platform. 
With the help of this library it is possible to draw objects instead of pixels.
For example, an object can be a Minecraft block and the drawing area can be a Minecraft chunk.
This makes it possible to create any graphical primitives, such as polygons, circles, etc. in a Minecraft map.

This library contains only the generic support.
A concrete example of how this can be done with Bukkit chunks is in [bukkit-graphics](https://gitlab.com/openstreetcraft/minecraft/bukkit-server/graphics).
