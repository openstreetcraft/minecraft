// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.location;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.ixilon.minecraft.MinecraftPlayer;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.api.service.LocationService;

public class LocationCommandExecutorTest {

  @Mock
  private CoordinatesService coordinatesService;

  @Mock
  private LocationService locationService;

  @Mock
  private MinecraftPlayer player;

  private LocationCommandExecutor executor;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    executor = new LocationCommandExecutor(coordinatesService, locationService);
  }

  @Test
  public void printsCurrentLocation() {
    World world = World.of("earth");
    MinecraftLocation currentMinecraftLocation = new MinecraftLocation(1, 2);
    SphericalLocation currentSphericalLocation = new SphericalLocation(3.4, 5.6);

    Mockito.when(player.getMinecraftLocation())
        .thenReturn(currentMinecraftLocation);

    Mockito.when(coordinatesService.getSphericalLocation(currentMinecraftLocation, world))
        .thenReturn(currentSphericalLocation);

    Mockito.when(locationService.getAddress(currentSphericalLocation))
        .thenReturn("foo");

    String[] args = {};

    executor.onCommand(player, args);

    Mockito.verify(player).sendMessage("foo");
  }

  @Test
  public void teleportsToLocation() {
    World world = World.of("earth");
    MinecraftLocation currentMinecraftLocation = new MinecraftLocation(1, 2);
    SphericalLocation expectedSphericalLocation = new SphericalLocation(3.4, 5.6);
    MinecraftLocation expectedMinecraftLocation = new MinecraftLocation(7, 8);

    Mockito.when(player.getMinecraftLocation())
        .thenReturn(currentMinecraftLocation);

    Mockito.when(coordinatesService.getMinecraftLocation(expectedSphericalLocation, world))
        .thenReturn(expectedMinecraftLocation);

    String[] args = { "3.4", "5.6" };

    executor.onCommand(player, args);

    Mockito.verify(player).setMinecraftLocation(expectedMinecraftLocation);
  }

  @Test
  public void teleportsToAddress() {
    World world = World.of("earth");
    MinecraftLocation currentMinecraftLocation = new MinecraftLocation(1, 2);
    SphericalLocation currentSphericalLocation = new SphericalLocation(3.4, 5.6);
    MinecraftLocation expectedMinecraftLocation = new MinecraftLocation(7, 8);
    SphericalLocation expectedSphericalLocation = new SphericalLocation(9.1, 12.1);

    Mockito.when(player.getMinecraftLocation())
        .thenReturn(currentMinecraftLocation);

    Mockito.when(coordinatesService.getSphericalLocation(currentMinecraftLocation, world))
        .thenReturn(currentSphericalLocation);

    Mockito.when(locationService.getLocation("Los Angeles", currentSphericalLocation))
        .thenReturn(expectedSphericalLocation);

    Mockito.when(coordinatesService.getMinecraftLocation(expectedSphericalLocation, world))
        .thenReturn(expectedMinecraftLocation);

    String[] args = { "Los", "Angeles" };

    executor.onCommand(player, args);

    Mockito.verify(player).setMinecraftLocation(expectedMinecraftLocation);
  }

}
