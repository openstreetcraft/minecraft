// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.world;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ChunkCoordinateTest {

  @Test
  public void calculatesNorthWestBlockCoordinate() {
    ChunkCoordinate chunk = new ChunkCoordinate(1, -2);
    BlockCoordinate expected = new BlockCoordinate(16, -32);
    BlockCoordinate actual = chunk.nw();
    assertEquals(expected, actual);
  }

  @Test
  public void calculatesNorthEastBlockCoordinate() {
    ChunkCoordinate chunk = new ChunkCoordinate(1, -2);
    BlockCoordinate expected = new BlockCoordinate(31, -32);
    BlockCoordinate actual = chunk.ne();
    assertEquals(expected, actual);
  }

  @Test
  public void calculatesSouthWestBlockCoordinate() {
    ChunkCoordinate chunk = new ChunkCoordinate(1, -2);
    BlockCoordinate expected = new BlockCoordinate(16, -17);
    BlockCoordinate actual = chunk.sw();
    assertEquals(expected, actual);
  }

  @Test
  public void calculatesSouthEastBlockCoordinate() {
    ChunkCoordinate chunk = new ChunkCoordinate(1, -2);
    BlockCoordinate expected = new BlockCoordinate(31, -17);
    BlockCoordinate actual = chunk.se();
    assertEquals(expected, actual);
  }

  @Test
  public void calculatesSouthEastBlockCoordinateWithScale() {
    ChunkCoordinate chunk = new ChunkCoordinate(1, -2, 6);
    BlockCoordinate expected = new BlockCoordinate(127, -65);
    BlockCoordinate actual = chunk.se();
    assertEquals(expected, actual);
  }

  @Test
  public void getsSizeFromScale() {
    assertEquals(16, new ChunkCoordinate(0, 0).size());
    assertEquals(16, new ChunkCoordinate(0, 0, 4).size());
    assertEquals(32, new ChunkCoordinate(0, 0, 5).size());
    assertEquals(64, new ChunkCoordinate(0, 0, 6).size());
  }

}
