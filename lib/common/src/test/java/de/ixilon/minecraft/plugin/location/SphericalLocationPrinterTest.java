// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.location;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Test;

import net.openstreetcraft.api.model.SphericalLocation;

public class SphericalLocationPrinterTest {
  private static final String DEGREE = "\u00B0"; // OK, degree character

  private SphericalLocationPrinter printer = new SphericalLocationPrinter();

  @Test
  public void printsNorthWestQuarterSphere() {
    SphericalLocation location = new SphericalLocation(12.34, -56.78);
    assertEquals("12.34" + DEGREE + "N 56.78" + DEGREE + "W", printer.print(location, Locale.US));
  }

  @Test
  public void printsNorthEastQuarterSphere() {
    SphericalLocation location = new SphericalLocation(12.34, 56.78);
    assertEquals("12.34" + DEGREE + "N 56.78" + DEGREE + "E", printer.print(location, Locale.US));
  }

  @Test
  public void printsSouthWestQuarterSphere() {
    SphericalLocation location = new SphericalLocation(-12.34, -56.78);
    assertEquals("12.34" + DEGREE + "S 56.78" + DEGREE + "W", printer.print(location, Locale.US));
  }

  @Test
  public void printsSouthEastQuarterSphere() {
    SphericalLocation location = new SphericalLocation(-12.34, 56.78);
    assertEquals("12.34" + DEGREE + "S 56.78" + DEGREE + "E", printer.print(location, Locale.US));
  }

  @Test
  public void printsZero() {
    SphericalLocation location = new SphericalLocation(0, 0);
    assertEquals("0" + DEGREE + "N 0" + DEGREE + "E", printer.print(location, Locale.US));
  }

}
