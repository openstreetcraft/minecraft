// Copyright (C) 2021 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.location;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;

import de.ixilon.minecraft.MinecraftPlayer;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.service.CoordinatesService;

public class LocationTabCompleterTest {
  
  private final World world = World.of("earth");
  
  @Mock
  private CoordinatesService coordinatesService;
  
  @Mock
  private MinecraftPlayer player;
  
  private LocationTabCompleter tabCompleter;
  
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    tabCompleter = new LocationTabCompleter(coordinatesService);
    
    MinecraftLocation currentMinecraftLocation = new MinecraftLocation(1, 2);
    SphericalLocation currentSphericalLocation = new SphericalLocation(3.4, 5.6);
    
    Mockito.when(player.getMinecraftLocation()).thenReturn(currentMinecraftLocation);
    Mockito.when(coordinatesService.getSphericalLocation(currentMinecraftLocation, world))
      .thenReturn(currentSphericalLocation);
  }
  
  @Test
  public void preventsCompletionIfThereIsNoArguments() {    
    String[] args = {};    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertNull(actual);
  }
  
  @Test
  public void autoCompleteFirstArgument() {    
    String[] args = { "3" };
    ImmutableList<String> expected = ImmutableList.of("3.4");
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertEquals(expected, actual);
  }
  
  @Test
  public void autoCompleteIfFirstArgumentIsEmpty() {    
    String[] args = { "" };
    ImmutableList<String> expected = ImmutableList.of("3.4");
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertEquals(expected, actual);
  }
  
  @Test
  public void preventsCompletionWhenTheUsersStartsToTypeAnOrdinaryAddress() {    
    String[] args = { "asd" };    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertNull(actual);
  }
  
  @Test
  public void preventsCompletionWhenTheFirstArgumentNotMatchUserLatitude() {
    String[] args = { "54.23" };
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertNull(actual);
  }
  
  @Test
  public void autoCompleteSecondArgument() {    
    String[] args = { "3.4", "" };
    ImmutableList<String> expected = ImmutableList.of("5.6");
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertEquals(expected, actual);
  }
  
  @Test
  public void autoCompleteSecondArgumentIfTheLongitudePartiallyMatchsUserLongitude() {
    String[] args = { "3.4", "5" };
    ImmutableList<String> excepted = ImmutableList.of("5.6");
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertEquals(excepted, actual);
  }
  
  @Test
  public void preventsSecondArgumentCompletionIfFirstArgumentNotMatchUserLatitude() {
    String[] args = { "54.23", "" };
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertNull(actual);
  }
  
  @Test
  public void preventsCompletionIfSecondArgumentNotMAtchUserLongitude() {
    String[] args = { "3.4", "64" };
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertNull(actual);
  }
  
  @Test
  public void preventsCompletionIfAThirdArgumentIsEntered() {    
    String[] args = { "3.4", "5.6", "" };
    
    ImmutableList<String> actual = tabCompleter.onTabComplete(player, args);
    
    assertNull(actual);
  }
  
}
