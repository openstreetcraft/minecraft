// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.world;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import net.openstreetcraft.api.model.MinecraftLocation;

public class BlockCoordinateTest {
  @Test
  public void getsChunkCoordinate() {
    BlockCoordinate blockCoordinate = new BlockCoordinate(815, 4711);
    ChunkCoordinate expected = new ChunkCoordinate(50, 294);
    ChunkCoordinate actual = blockCoordinate.getChunkCoordinate();
    assertEquals(expected, actual);
  }

  @Test
  public void getsChunkCoordinateWithScale() {
    BlockCoordinate blockCoordinate = new BlockCoordinate(815, 4711);
    ChunkCoordinate expected = new ChunkCoordinate(12, 73, 6);
    ChunkCoordinate actual = blockCoordinate.getChunkCoordinate(6);
    assertEquals(expected, actual);
  }

  @Test
  public void constructsFromMinecraftLocation() {
    MinecraftLocation location = new MinecraftLocation(-4L, 2L);
    BlockCoordinate coordinate = new BlockCoordinate(location);
    assertEquals(-4, coordinate.getX());
    assertEquals(2, coordinate.getZ());
  }

  @Test
  public void convertsToMinecraftLocation() {
    BlockCoordinate coordinate = new BlockCoordinate(4, -2);
    MinecraftLocation expected = new MinecraftLocation(4L, -2L);
    MinecraftLocation actual = coordinate.toMinecraftLocation();
    assertEquals(expected, actual);
  }
}
