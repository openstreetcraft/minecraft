// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.terrain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Test;

import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.model.Cover;
import net.openstreetcraft.minecraft.biome.Biome;

public class TerrainImplTest {

  @Test
  public void replacesBiomeByElevation() {
    Stream.of(Biome.values()).forEach(biome -> {
      final Biome expected;
      switch (biome) {
        case COLD_OCEAN:
          expected = Biome.DEEP_COLD_OCEAN;
          break;
        case FROZEN_OCEAN:
          expected = Biome.DEEP_FROZEN_OCEAN;
          break;
        case LUKEWARM_OCEAN:
          expected = Biome.DEEP_LUKEWARM_OCEAN;
          break;
        case WARM_OCEAN:
          expected = Biome.DEEP_OCEAN;
          break;
        case OCEAN:
          expected = Biome.DEEP_OCEAN;
          break;
        default:
          expected = biome;
      }
      Biome actual = TerrainImpl.replaceBiomeByElevation(biome, -60);
      assertEquals(biome.toString(), expected, actual);
    });
  }

  @Test
  public void replacesBiomeByIceCover() {
    Stream.of(Biome.values()).forEach(biome -> {
      final Biome expected;
      switch (biome) {
        case OCEAN:
          expected = Biome.FROZEN_OCEAN;
          break;
        case RIVER:
          expected = Biome.FROZEN_RIVER;
          break;
        default:
          expected = biome;
      }
      Biome actual = TerrainImpl.replaceBiomeByCover(biome, Cover.ICE);
      assertEquals(biome.toString(), expected, actual);
    });
  }

  @Test
  public void ignoresOtherBiomesByLatitude() {
    Stream.of(Biome.values()).forEach(biome -> {
      switch (biome) {
        case OCEAN:
          // ignore
          break;
        default:
          assertEquals(biome, TerrainImpl.replaceBiomeByLatitude(biome, 0));
      }
    });
  }

  @Test
  public void replacesOceanBiomeByLatitudeAtPolarZone() {
    Biome expected = Biome.FROZEN_OCEAN;

    IntStream.range(81, 90).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 80));
    
    IntStream.range(-81, -90).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -80));
  }

  @Test
  public void replacesOceanBiomeByLatitudeAtSubpolarZone() {
    Biome expected = Biome.COLD_OCEAN;

    IntStream.range(61, 80).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 60));
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 81));
    
    IntStream.range(-61, -80).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -60));
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -81));
  }

  @Test
  public void replacesOceanBiomeByLatitudeAtTemperateZone() {
    Biome expected = Biome.OCEAN;

    IntStream.range(41, 60).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 40));
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 61));
    
    IntStream.range(-41, -60).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -40));
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -61));
  }

  @Test
  public void replacesOceanBiomeByLatitudeAtSubtropicsZone() {
    Biome expected = Biome.LUKEWARM_OCEAN;

    IntStream.range(24, 40).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 23));
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 41));
    
    IntStream.range(-24, -40).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -23));
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -41));
  }

  @Test
  public void replacesOceanBiomeByLatitudeAtTropicsZone() {
    Biome expected = Biome.WARM_OCEAN;

    IntStream.range(-23, 23).forEach(latitude -> {
      Biome actual = TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, latitude);
      assertEquals(Integer.toString(latitude), expected, actual);
    });
    
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, -24));
    assertNotEquals(expected, TerrainImpl.replaceBiomeByLatitude(Biome.OCEAN, 24));
  }
  
  @Test
  public void addsPlasticPollution() {
    Raster<Byte> samples1 = new Raster<Byte>(2, 2, new Byte[] {0, 1, 2, 3});
    Raster<Byte> samples2 = new Raster<Byte>(2, 2, new Byte[] {4, 3, 2, -1});
    Raster<Float> expected =
        new Raster<Float>(2, 2, new Float[] {10001.0F, 1010.0F, 200.0F, 1000.1F});
    Raster<Float> actual = TerrainImpl.addPlastic(samples1, samples2);
    assertEquals(expected, actual);
  }

}
