// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.timezone;

import static org.junit.Assert.assertEquals;

import java.time.ZonedDateTime;

import org.junit.Test;

public class TimezoneUtilsTest {

  @Test
  public void convertsSunriseToTicks() {
    ZonedDateTime time = ZonedDateTime.parse("2017-08-14T06:00+02:00");
    int actual = TimezoneUtils.toTicks(time);
    assertEquals(0, actual);
  }

  @Test
  public void convertsNoonToTicks() {
    ZonedDateTime time = ZonedDateTime.parse("2017-08-14T12:00+02:00");
    int actual = TimezoneUtils.toTicks(time);
    assertEquals(6000, actual);
  }

  @Test
  public void convertsSunsetToTicks() {
    ZonedDateTime time = ZonedDateTime.parse("2017-08-14T18:00+02:00");
    int actual = TimezoneUtils.toTicks(time);
    assertEquals(12000, actual);
  }

  @Test
  public void convertsMidnightToTicks() {
    ZonedDateTime time = ZonedDateTime.parse("2017-08-14T00:00:00+02:00");
    int actual = TimezoneUtils.toTicks(time);
    assertEquals(18000, actual);
  }

}
