// Copyright (C) 2018 Wout Ceulemans <wout.ceulemans@student.kuleuven.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.timezone;

import java.time.ZonedDateTime;

public class TimezoneUtils {
  /**
   * The amount of hours in 1 day.
   */
  static final int HOURS_PER_DAY = 24;

  /**
   * The amount of minutes in 1 hour.
   */
  static final int MINUTES_PER_HOUR = 60;

  /**
   * The amount of seconds in 1 hour.
   */
  static final int SECONDS_PER_HOUR = 3600;

  /**
   * The amount of ticks in 1 second.
   * This is an estimation, slow servers might cause the actual number to drop.
   * Don't use this if its important to know the actual TPS.
   */
  static final int TICKS_PER_SECOND = 20;

  /**
   * The amount of ticks in 1 Minecraft hour.
   */
  static final int TICKS_PER_MINECRAFT_HOUR = 1000;

  /**
   * The amount of ticks that is seen as "midnight" for Minecraft.
   */
  static final int TICKS_AT_MIDNIGHT = 18 * TICKS_PER_MINECRAFT_HOUR;

  /**
   * The amount of ticks in 1 Minecraft day.
   */
  static final int TICKS_PER_MINECRAFT_DAY = HOURS_PER_DAY * TICKS_PER_MINECRAFT_HOUR;
  
  private TimezoneUtils() {
    throw new UnsupportedOperationException();
  }

  /**
   * Converts the ZonedDateTime in minecraft ticks.
   * The current algorithm calculates it's minecraft tick equivalent in hours and minutes.
   * @param time the ZonedDateTime to convert
   * @return The converted ZonedDateTime in ticks
   */
  public static int toTicks(ZonedDateTime time) {
    int ticks = TICKS_AT_MIDNIGHT;

    ticks += time.getHour() * TICKS_PER_MINECRAFT_HOUR;
    ticks += (time.getMinute() / MINUTES_PER_HOUR) * TICKS_PER_MINECRAFT_HOUR;
    ticks %= TICKS_PER_MINECRAFT_DAY;

    return ticks;
  }
}
