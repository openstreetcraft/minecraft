// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.world;

import net.openstreetcraft.api.geom.BoundingBox;
import net.openstreetcraft.api.geom.Point;

public class MinecraftBoundingBox extends BoundingBox {

  /**
   * Create a bounding box of a chunk.
   */
  public MinecraftBoundingBox(ChunkCoordinate coordinate) {
    super(new Builder()
        .withLowerLeft(new Point(coordinate.sw().getX(), coordinate.sw().getZ() + 1))
        .withLowerRight(new Point(coordinate.se().getX() + 1, coordinate.se().getZ() + 1))
        .withUpperLeft(new Point(coordinate.nw().getX(), coordinate.nw().getZ()))
        .withUpperRight(new Point(coordinate.ne().getX() + 1, coordinate.ne().getZ())));
  }

  /**
   * Create a bounding box of a block.
   */
  public MinecraftBoundingBox(BlockCoordinate coordinate) {
    super(new Builder()
        .withLowerLeft(new Point(coordinate.getX(), coordinate.getZ() + 1))
        .withLowerRight(new Point(coordinate.getX() + 1, coordinate.getZ() + 1))
        .withUpperLeft(new Point(coordinate.getX(), coordinate.getZ()))
        .withUpperRight(new Point(coordinate.getX() + 1, coordinate.getZ())));
  }
}
