// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.location;

import java.util.Locale;

import org.springframework.format.Printer;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Joiner;
import com.google.common.primitives.Doubles;

import de.ixilon.minecraft.MinecraftPlayer;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.api.service.LocationService;

public class LocationCommandExecutor {

  private static final World WORLD = World.of("earth");

  private final CoordinatesService coordinatesService;
  private final LocationService locationService;

  public LocationCommandExecutor(ClassLoader classLoader) {
    this(createRestTemplate(classLoader));
  }

  public LocationCommandExecutor(RestTemplate restTemplate) {
    this(new CoordinatesService(restTemplate), new LocationService(restTemplate));
  }

  public LocationCommandExecutor(CoordinatesService coordinatesService,
      LocationService locationService) {
    this.coordinatesService = coordinatesService;
    this.locationService = locationService;
  }

  private static RestTemplate createRestTemplate(ClassLoader classLoader) {
    RestTemplateFactory factory = new RestTemplateFactory(classLoader);
    return factory.getRestTemplate();
  }

  /**
   * Executes the command with the given arguments for a concrete player.
   */
  public void onCommand(MinecraftPlayer player, String[] args) {
    if (args.length == 0) {
      printCurrentLocation(player);
    } else if (isDoublePair(args)) {
      teleportToLocation(player, args);
    } else {
      teleportToAddress(player, args);
    }
  }

  private void printCurrentLocation(MinecraftPlayer player) {
    SphericalLocation location = currentLocation(player);
    Printer<SphericalLocation> printer = new SphericalLocationPrinter();

    try {
      player.sendMessage(printer.print(location, Locale.US));
      player.sendMessage(locationService.getAddress(location));
    } catch (RestClientException exception) {
      player.sendMessage(exception.getMessage());
    }
  }

  private SphericalLocation currentLocation(MinecraftPlayer player) {
    return coordinatesService.getSphericalLocation(player.getMinecraftLocation(), WORLD);
  }

  private static boolean isDoublePair(String[] args) {
    return (args.length == 2) && isDouble(args[0]) && isDouble(args[1]);
  }

  private static boolean isDouble(String text) {
    return Doubles.tryParse(text) != null;
  }

  private void teleportToLocation(MinecraftPlayer player, String[] args) {
    double latitude = Double.parseDouble(args[0]);
    double longitude = Double.parseDouble(args[1]);
    teleportToLocation(player, new SphericalLocation(latitude, longitude));
  }

  private void teleportToLocation(MinecraftPlayer player, SphericalLocation location) {
    teleportToLocation(player, coordinatesService.getMinecraftLocation(location, WORLD));
  }

  private void teleportToLocation(MinecraftPlayer player, MinecraftLocation location) {
    player.setMinecraftLocation(location);
  }

  private void teleportToAddress(MinecraftPlayer player, String[] args) {
    String address = Joiner.on(" ").join(args);
    try {
      teleportToLocation(player, locationService.getLocation(address, currentLocation(player)));
    } catch (RestClientException exception) {
      player.sendMessage(exception.getMessage());
    }
  }
}
