// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.biome;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.geom.RectangularBoundingBox;
import net.openstreetcraft.api.model.Layer;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.MapService;
import net.openstreetcraft.minecraft.biome.Biome;
import net.openstreetcraft.minecraft.biome.BiomeConverter;

public class BiomeService {
  private static final World WORLD = World.of("earth");
  private static final BiomeConverter CONVERTER = new BiomeConverter();

  private final MapService mapService;

  public BiomeService(ClassLoader classLoader) {
    this(createRestTemplate(classLoader));
  }

  public BiomeService(RestTemplate restTemplate) {
    this(new MapService(restTemplate));
  }

  public BiomeService(MapService mapService) {
    this.mapService = mapService;
  }

  private static RestTemplate createRestTemplate(ClassLoader classLoader) {
    RestTemplateFactory factory = new RestTemplateFactory(classLoader);
    return factory.getRestTemplate();
  }

  /**
   * Returns width * height biomes within a given bounding box of spherical coordinates.
   */
  public Raster<Biome> biomes(RectangularBoundingBox bbox, int width, int height)
      throws RestClientException {
    return mapService.getByteSamples(WORLD, Layer.BIOME, bbox, width, height).convert(CONVERTER);
  }
}
