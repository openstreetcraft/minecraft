// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft;

import net.openstreetcraft.api.model.MinecraftLocation;

public interface MinecraftPlayer {
  
  /**
   * Sends a text message to the player.
   */
  public void sendMessage(String message);

  /**
   * Returns the current location of the player.
   */
  public MinecraftLocation getMinecraftLocation();
  
  /**
   * Teleports the player to a given location.
   */
  public void setMinecraftLocation(MinecraftLocation location);
  
}
