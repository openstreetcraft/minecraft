// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.elevation;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.geom.RectangularBoundingBox;
import net.openstreetcraft.api.model.Layer;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.MapService;

public class ElevationService {
  private static final World WORLD = World.of("earth");

  private final MapService mapService;

  public ElevationService(ClassLoader classLoader) {
    this(createRestTemplate(classLoader));
  }

  public ElevationService(RestTemplate restTemplate) {
    this(new MapService(restTemplate));
  }

  public ElevationService(MapService mapService) {
    this.mapService = mapService;
  }

  private static RestTemplate createRestTemplate(ClassLoader classLoader) {
    RestTemplateFactory factory = new RestTemplateFactory(classLoader);
    return factory.getRestTemplate();
  }

  /**
   * Returns width * height heights within a given bounding box of spherical coordinates.
   */
  public Raster<Integer> elevations(RectangularBoundingBox bbox, int width, int height)
      throws RestClientException {
    return mapService.getFloatSamples(WORLD, Layer.ELEVATION, bbox, width, height)
        .convert(v -> (int) Math.floor(v));
  }
}
