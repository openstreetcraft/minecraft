// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.world;

import java.util.Objects;

public class Coordinate {
  private final int xcoord;
  private final int zcoord;

  protected Coordinate(int xcoord, int zcoord) {
    this.xcoord = xcoord;
    this.zcoord = zcoord;
  }
  
  public Coordinate add(Coordinate other) {
    return new Coordinate(xcoord + other.xcoord, zcoord + other.zcoord);
  }

  public Coordinate sub(Coordinate other) {
    return new Coordinate(xcoord - other.xcoord, zcoord - other.zcoord);
  }

  @Override
  public int hashCode() {
    return Objects.hash(xcoord, zcoord);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Coordinate)) {
      return false;
    }

    Coordinate other = (Coordinate) obj;

    return Objects.equals(this.xcoord, other.xcoord)
        && Objects.equals(this.zcoord, other.zcoord);
  }

  @Override
  public String toString() {
    return "Coordinate [x=" + xcoord + ", z=" + zcoord + "]";
  }

  public int getX() {
    return xcoord;
  }

  public int getZ() {
    return zcoord;
  }
}
