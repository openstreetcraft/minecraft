// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.terrain;

import net.openstreetcraft.api.model.Cover;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.minecraft.biome.Biome;

public interface Terrain {

  /**
   * The Minecraft y-coordinate of the highest block.
   */
  int surface(World world, MinecraftLocation location);

  /**
   * The biome at the given Minecraft location.
   */
  Biome biome(World world, MinecraftLocation location);

  /**
   * The cover data at the given Minecraft location.
   */
  Cover cover(World world, MinecraftLocation location);

  /**
   * The spherical location at the given Minecraft location.
   */
  SphericalLocation sphericalLocation(World world, MinecraftLocation location);

  /**
   * Plastic pollution at the given Minecraft location in pieces per km².
   */
  float plastic(World world, MinecraftLocation location);

}
