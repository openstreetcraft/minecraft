// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.biome;

import javax.naming.OperationNotSupportedException;

import net.openstreetcraft.minecraft.biome.Biome;

public class Biomes {

  private Biomes() throws OperationNotSupportedException {
    throw new OperationNotSupportedException("static utility class");
  }

  /**
   * @return true if this is a water biome.
   */
  public static boolean isWater(Biome biome) {
    return isRiver(biome) || isOcean(biome);
  }

  /**
   * @return true if this is a river biome.
   */
  public static boolean isRiver(Biome biome) {
    switch (biome) {
      case FROZEN_RIVER:
      case RIVER:
        return true;
      default:
        return false;
    }
  }

  /**
   * @return true if this is an ocean biome.
   */
  public static boolean isOcean(Biome biome) {
    switch (biome) {
      case COLD_OCEAN:
      case DEEP_COLD_OCEAN:
      case DEEP_FROZEN_OCEAN:
      case DEEP_LUKEWARM_OCEAN:
      case DEEP_OCEAN:
      // case DEEP_WARM_OCEAN:
      case FROZEN_OCEAN:
      case LUKEWARM_OCEAN:
      case OCEAN:
      case WARM_OCEAN:
        return true;
      default:
        return false;
    }
  }
  
  /**
   * @return true if this is a forest biome.
   */
  public static boolean isForest(Biome biome) {
    switch (biome) {
      case BAMBOO_JUNGLE:
      // case BAMBOO_JUNGLE_HILLS:
      case BIRCH_FOREST:
      // case BIRCH_FOREST_HILLS:
      case CRIMSON_FOREST:
      case FLOWER_FOREST:
      case FOREST:
      case JUNGLE:
      // case JUNGLE_EDGE:
      // case JUNGLE_HILLS:
      case TAIGA:
      // case TAIGA_HILLS:
        return true;
      default:
        return false;
    }
  }
}
