// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.location;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.springframework.format.Printer;

import net.openstreetcraft.api.model.SphericalLocation;

public class SphericalLocationPrinter implements Printer<SphericalLocation> {

  @Override
  public String print(SphericalLocation location, Locale locale) {
    NumberFormat formatter = DecimalFormat.getInstance(locale);
    return print(formatter, location.getLatitude(), "N", "S")
        + " "
        + print(formatter, location.getLongitude(), "E", "W");
  }
  
  private static String print(NumberFormat formatter, double value, String pos, String neg) {
    String degree = "\u00B0"; // OK, degree character
    return formatter.format(Math.abs(value)) + degree + (value < 0 ? neg : pos);
  }

}
