// Copyright (C) 2021 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.location;

import org.springframework.web.client.RestTemplate;

import com.google.common.collect.ImmutableList;

import de.ixilon.minecraft.MinecraftPlayer;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.api.service.CoordinatesService;

public class LocationTabCompleter {
  
  private static final World WORLD = World.of("earth");

  private final CoordinatesService coordinatesService;
  
  public LocationTabCompleter(ClassLoader classLoader) {
    this(createRestTemplate(classLoader));
  }

  public LocationTabCompleter(RestTemplate restTemplate) {
    this(new CoordinatesService(restTemplate));
  }

  public LocationTabCompleter(CoordinatesService coordinatesService) {
    this.coordinatesService = coordinatesService;
  }

  private static RestTemplate createRestTemplate(ClassLoader classLoader) {
    RestTemplateFactory factory = new RestTemplateFactory(classLoader);
    return factory.getRestTemplate();
  }
  
  /**
   * Completes arguments for the location command.
   */
  public ImmutableList<String> onTabComplete(MinecraftPlayer player, String[] args) {
    if (args.length == 0 || (!isDouble(args[0]) && !args[0].equals(""))) {
      return null;
    }
    
    SphericalLocation location = currentLocation(player);
    
    switch (args.length) {
      case 1:
        if (!startsWith(args[0], location.getLatitude())) {
          return null;
        }
        
        return ImmutableList.of(String.valueOf(location.getLatitude()));
      
      case 2:
        if (!(startsWith(args[1], location.getLongitude()))) {
          return null;
        }
        
        if (Double.parseDouble(args[0]) != location.getLatitude()) {
          return null;
        }
        
        return ImmutableList.of(String.valueOf(location.getLongitude()));
      
      default:
        return null;
    }
  }
  
  private SphericalLocation currentLocation(MinecraftPlayer player) {
    return coordinatesService.getSphericalLocation(player.getMinecraftLocation(), WORLD);
  }
  
  private boolean isDouble(String string) {    
    try {
      Double.parseDouble(string);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }
  
  private boolean startsWith(String str, double number) {
    return Double.toString(number).startsWith(str);
  }
}
