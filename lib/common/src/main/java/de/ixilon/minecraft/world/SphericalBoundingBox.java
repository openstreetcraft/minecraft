// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.world;

import net.openstreetcraft.api.geom.BoundingBox;
import net.openstreetcraft.api.geom.Point;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.service.SphericalLocationLoader;

public class SphericalBoundingBox extends BoundingBox {

  public SphericalBoundingBox(MinecraftBoundingBox bbox, SphericalLocationLoader loader) {
    super(builder(bbox, loader));
  }

  private static Builder builder(MinecraftBoundingBox bbox, SphericalLocationLoader loader) {
    MinecraftLocation lowerLeft = toMinecraftLocation(bbox.getLowerLeft());
    MinecraftLocation lowerRight = toMinecraftLocation(bbox.getLowerRight());
    MinecraftLocation upperLeft = toMinecraftLocation(bbox.getUpperLeft());
    MinecraftLocation upperRight = toMinecraftLocation(bbox.getUpperRight());
    
    loader.invalidate();
    
    loader.prefetch(lowerLeft);
    loader.prefetch(lowerRight);
    loader.prefetch(upperLeft);
    loader.prefetch(upperRight);
    
    return new Builder()
        .withLowerLeft(toPoint(loader.get(lowerLeft)))
        .withLowerRight(toPoint(loader.get(lowerRight)))
        .withUpperLeft(toPoint(loader.get(upperLeft)))
        .withUpperRight(toPoint(loader.get(upperRight)));
  }

  private static MinecraftLocation toMinecraftLocation(Point point) {
    return new MinecraftLocation(toLong(point.getX()), toLong(point.getY()));
  }

  private static long toLong(double value) {
    return Math.round(value);
  }

  private static Point toPoint(SphericalLocation location) {
    return new Point(location.getLongitude(), location.getLatitude());
  }
}
