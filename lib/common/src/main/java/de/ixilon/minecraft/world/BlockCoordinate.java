// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.world;

import net.openstreetcraft.api.model.MinecraftLocation;

public class BlockCoordinate extends Coordinate {
  public BlockCoordinate(int xcoord, int zcoord) {
    super(xcoord, zcoord);
  }

  public BlockCoordinate(MinecraftLocation location) {
    this(toInt(location.getX()), toInt(location.getZ()));
  }
  
  private static int toInt(long value) {
    return Math.toIntExact(value);
  }

  public ChunkCoordinate getChunkCoordinate() {
    return getChunkCoordinate(4);
  }
  
  public ChunkCoordinate getChunkCoordinate(int scale) {
    return new ChunkCoordinate(getX() >> scale, getZ() >> scale, scale);
  }
  
  public MinecraftLocation toMinecraftLocation() {
    return new MinecraftLocation(getX(), getZ());
  }
  
  @Override
  public String toString() {
    return "BlockCoordinate [x=" + getX() + ", z=" + getZ() + "]";
  }
}
