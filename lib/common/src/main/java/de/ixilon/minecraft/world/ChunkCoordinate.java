// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.world;

import java.util.Objects;

public class ChunkCoordinate extends Coordinate {
  private final int scale;
  private final int size;

  /**
   * Minecraft chunk coordinates are 1/16 of block coordinates.
   */
  public ChunkCoordinate(int xcoord, int zcoord) {
    super(xcoord, zcoord);
    this.scale = 4;
    this.size = 16;
  }

  /**
   * Chunk coordinates with 1/2^scale of block coordinates.
   * The default scale for Minecraft is 4.
   */
  public ChunkCoordinate(int xcoord, int zcoord, int scale) {
    super(xcoord, zcoord);
    this.scale = scale;
    this.size = (int) Math.pow(2, scale);
  }

  public int size() {
    return size;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), size);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ChunkCoordinate)) {
      return false;
    }
    ChunkCoordinate other = (ChunkCoordinate) obj;
    return super.equals(this) && Objects.equals(this.size, other.size);
  }


  @Override
  public String toString() {
    return "ChunkCoordinate [x=" + getX() + ", z=" + getZ() + "]";
  }

  public BlockCoordinate nw() {
    return new BlockCoordinate(west(), north());
  }

  public BlockCoordinate ne() {
    return new BlockCoordinate(west() + size - 1, north());
  }

  public BlockCoordinate sw() {
    return new BlockCoordinate(west(), north() + size - 1);
  }

  public BlockCoordinate se() {
    return new BlockCoordinate(west() + size - 1, north() + size - 1);
  }

  private int north() {
    return getZ() << scale;
  }

  private int west() {
    return getX() << scale;
  }
}
