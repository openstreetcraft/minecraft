// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>, Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.minecraft.plugin.terrain;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.springframework.web.client.RestTemplate;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;

import de.ixilon.minecraft.world.BlockCoordinate;
import de.ixilon.minecraft.world.ChunkCoordinate;
import de.ixilon.minecraft.world.Coordinate;
import de.ixilon.minecraft.world.MinecraftBoundingBox;
import de.ixilon.minecraft.world.SphericalBoundingBox;
import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.geom.RectangularBoundingBox;
import net.openstreetcraft.api.model.Cover;
import net.openstreetcraft.api.model.Layer;
import net.openstreetcraft.api.model.MinecraftLocation;
import net.openstreetcraft.api.model.SphericalLocation;
import net.openstreetcraft.api.model.World;
import net.openstreetcraft.api.service.Cache;
import net.openstreetcraft.api.service.CoordinatesService;
import net.openstreetcraft.api.service.MapService;
import net.openstreetcraft.api.service.SphericalLocationLoader;
import net.openstreetcraft.minecraft.biome.Biome;
import net.openstreetcraft.minecraft.biome.BiomeConverter;

public class TerrainImpl implements Terrain {
  private static final int SCALE = 7; // load 8x8 chunks at once ((scale-4)^2)
  private static final BiomeConverter biomeConverter = new BiomeConverter();
  private static final Function<Float, Integer> floatConverter = value -> (int) Math.floor(value);
  private static final Cover.Converter coverConverter = new Cover.Converter();

  private final MapService mapService;
  private final CoordinatesService coordinatesService;

  private final Map<Map.Entry<World, ChunkCoordinate>, Raster<Integer>> elevations;
  private final Map<Map.Entry<World, ChunkCoordinate>, Raster<Biome>> biomes;
  private final Map<Map.Entry<World, ChunkCoordinate>, Raster<Cover>> cover;
  private final Map<Map.Entry<World, ChunkCoordinate>, RectangularBoundingBox> bboxes;
  private final Map<Map.Entry<World, ChunkCoordinate>, Raster<SphericalLocation>> locations;
  private final Map<Map.Entry<World, ChunkCoordinate>, Raster<Float>> plastics;

  private Map<World, SphericalLocationLoader> sphericalLocationLoaders = new HashMap<>();

  /**
   * Create terrain API.
   * 
   * @param cacheSize Maximum number of chunks during world generation.
   */
  public TerrainImpl(RestTemplate restTemplate, int cacheSize) {
    mapService = new MapService(restTemplate);
    coordinatesService = new CoordinatesService(restTemplate);
    elevations = Collections.synchronizedMap(new Cache<>(cacheSize));
    biomes = Collections.synchronizedMap(new Cache<>(cacheSize));
    cover = Collections.synchronizedMap(new Cache<>(cacheSize));
    bboxes = Collections.synchronizedMap(new Cache<>(cacheSize));
    locations = Collections.synchronizedMap(new Cache<>(cacheSize));
    plastics = Collections.synchronizedMap(new Cache<>(cacheSize));
  }

  /**
   * Query a sample within a generic raster of the containing chunk.
   */
  private static <T> T getSample(World world, MinecraftLocation location,
      BiFunction<World, ChunkCoordinate, Raster<T>> rasterProvider) {
    BlockCoordinate block = new BlockCoordinate(location);
    ChunkCoordinate chunk = block.getChunkCoordinate(SCALE);
    Raster<T> raster = rasterProvider.apply(world, chunk);
    Coordinate inchunk = block.sub(chunk.nw());
    return raster.getSample(inchunk.getX(), inchunk.getZ());
  }

  @Override
  public int surface(World world, MinecraftLocation location) {
    return getSample(world, location, this::elevationRaster);
  }

  private Raster<Integer> elevationRaster(World world, ChunkCoordinate chunkCoordinate) {
    return elevations.computeIfAbsent(key(world, chunkCoordinate),
        key -> mapService.getFloatSamples(world, Layer.ELEVATION,
            boundingBox(world, chunkCoordinate), chunkCoordinate.size(), chunkCoordinate.size())
            .convert(floatConverter));
  }

  @Override
  public Biome biome(World world, MinecraftLocation location) {
    Biome biome = getSample(world, location, this::biomeRaster);
    return replaceBiome(biome, world, location);
  }

  private Raster<Biome> biomeRaster(World world, ChunkCoordinate chunkCoordinate) {
    return biomes.computeIfAbsent(key(world, chunkCoordinate),
        key -> mapService.getByteSamples(world, Layer.BIOME, boundingBox(world, chunkCoordinate),
            chunkCoordinate.size(), chunkCoordinate.size()).convert(biomeConverter));
  }

  private Biome replaceBiome(Biome biome, World world, MinecraftLocation location) {
    if (world != World.EARTH) {
      return biome;
    }
    if (biome == Biome.OCEAN || biome == Biome.RIVER) {
      biome = replaceBiomeByCover(biome, cover(world, location));
    }
    if (biome == Biome.OCEAN) {
      double latitude = sphericalLocation(world, location).getLatitude();
      biome = replaceBiomeByLatitude(biome, latitude);
    }
    if (biome == Biome.OCEAN || biome == Biome.WARM_OCEAN || biome == Biome.LUKEWARM_OCEAN
        || biome == Biome.COLD_OCEAN || biome == Biome.FROZEN_OCEAN) {
      biome = replaceBiomeByElevation(biome, surface(world, location));
    }
    return biome;
  }

  @VisibleForTesting
  static Biome replaceBiomeByCover(Biome biome, Cover cover) {
    switch (cover) {
      case ICE:
        switch (biome) {
          case OCEAN:
            return Biome.FROZEN_OCEAN;
          case RIVER:
            return Biome.FROZEN_RIVER;
          default:
            return biome;
        }
      default:
        return biome;
    }
  }

  @VisibleForTesting
  static Biome replaceBiomeByLatitude(Biome biome, double latitude) {
    switch (biome) {
      case OCEAN:
        if (Range.closed(-23.5, 23.5).contains(latitude)) {
          return Biome.WARM_OCEAN;
        }
        if (Range.closed(-40.0, 40.0).contains(latitude)) {
          return Biome.LUKEWARM_OCEAN;
        }
        if (Range.closed(-60.0, 60.0).contains(latitude)) {
          return Biome.OCEAN;
        }
        if (Range.closed(-80.0, 80.0).contains(latitude)) {
          return Biome.COLD_OCEAN;
        }
        if (Range.closed(-90.0, 90.0).contains(latitude)) {
          return Biome.FROZEN_OCEAN;
        }
        return biome;
      default:
        return biome;
    }
  }

  @VisibleForTesting
  static Biome replaceBiomeByElevation(Biome biome, int elevation) {
    if (elevation > -60) {
      return biome;
    }
    switch (biome) {
      case OCEAN:
      case WARM_OCEAN:
        return Biome.DEEP_OCEAN;
      case LUKEWARM_OCEAN:
        return Biome.DEEP_LUKEWARM_OCEAN;
      case COLD_OCEAN:
        return Biome.DEEP_COLD_OCEAN;
      case FROZEN_OCEAN:
        return Biome.DEEP_FROZEN_OCEAN;
      default:
        return biome;
    }
  }

  @Override
  public Cover cover(World world, MinecraftLocation location) {
    return getSample(world, location, this::coverRaster);
  }

  private Raster<Cover> coverRaster(World world, ChunkCoordinate chunkCoordinate) {
    return cover.computeIfAbsent(key(world, chunkCoordinate),
        key -> mapService.getByteSamples(world, Layer.COVER, boundingBox(world, chunkCoordinate),
            chunkCoordinate.size(), chunkCoordinate.size()).convert(coverConverter));
  }

  @Override
  public SphericalLocation sphericalLocation(World world, MinecraftLocation location) {
    return getSample(world, location, this::sphericalRaster);
  }

  private Raster<SphericalLocation> sphericalRaster(World world, ChunkCoordinate chunkCoordinate) {
    return locations.computeIfAbsent(key(world, chunkCoordinate),
        key -> toRaster(
            coordinatesService.getSphericalLocations(minecraftLocations(chunkCoordinate), world),
            SphericalLocation.class));
  }

  private static <T> Raster<T> toRaster(List<T> list, Class<T> objectClass) {
    int width = (int) Math.sqrt(list.size());
    return new Raster<>(width, width, toArray(list, objectClass));
  }

  private static <T> T[] toArray(List<T> list, Class<T> objectClass) {
    T[] array = (T[]) Array.newInstance(objectClass, list.size());
    list.toArray(array);
    return array;
  }

  @Override
  public float plastic(World world, MinecraftLocation location) {
    return getSample(world, location, this::plasticRaster);
  }

  private Raster<Float> plasticRaster(World world, ChunkCoordinate chunkCoordinate) {
    return plastics.computeIfAbsent(key(world, chunkCoordinate), key -> {
      RectangularBoundingBox boundingBox = boundingBox(world, chunkCoordinate);
      int size = chunkCoordinate.size();
      return addPlastic(
          mapService.getByteSamples(world, Layer.PLASTIC_C2, boundingBox, size, size),
          mapService.getByteSamples(world, Layer.PLASTIC_C3, boundingBox, size, size),
          mapService.getByteSamples(world, Layer.PLASTIC_C4, boundingBox, size, size));
    });
  }

  @VisibleForTesting
  @SafeVarargs
  static Raster<Float> addPlastic(Raster<Byte>... samples) {
    int width = samples[0].getWidth();
    int height = samples[0].getHeight();
    Float[] data = new Float[width * height]; 
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        int index = x + y * width;
        data[index] = 0F;
        for (int sample = 0; sample < samples.length; sample++) {
          data[index] += (float) Math.pow(10, samples[sample].getSample(x, y));
        }
      }
    }
    return new Raster<Float>(width, height, data);
  }
  
  private static ImmutableList<MinecraftLocation> minecraftLocations(
      ChunkCoordinate chunkCoordinate) {
    ImmutableList.Builder<MinecraftLocation> result = ImmutableList.builder();
    BlockCoordinate origin = chunkCoordinate.nw();
    for (int dx = 0; dx < chunkCoordinate.size(); dx++) {
      for (int dz = 0; dz < chunkCoordinate.size(); dz++) {
        result.add(new MinecraftLocation(origin.getX() + dx, origin.getZ() + dz));
      }
    }
    return result.build();
  }

  private RectangularBoundingBox boundingBox(World world, ChunkCoordinate chunkCoordinate) {
    return bboxes.computeIfAbsent(key(world, chunkCoordinate), key -> {
      MinecraftBoundingBox bbox = new MinecraftBoundingBox(chunkCoordinate);
      SphericalLocationLoader loader = getSphericalLocationLoader(world);
      return toRectangularBoundingBox(new SphericalBoundingBox(bbox, loader));
    });
  }

  private static Map.Entry<World, ChunkCoordinate> key(World world, ChunkCoordinate coordinate) {
    return new AbstractMap.SimpleEntry<>(world, coordinate);
  }

  private static RectangularBoundingBox toRectangularBoundingBox(SphericalBoundingBox bbox) {
    return new RectangularBoundingBox.Builder()
        .withLowerLeft(bbox.getLowerLeft())
        .withUpperRight(bbox.getUpperRight())
        .build();
  }

  private SphericalLocationLoader getSphericalLocationLoader(World world) {
    return sphericalLocationLoaders.computeIfAbsent(world,
        key -> new SphericalLocationLoader(coordinatesService, key));
  }

}
