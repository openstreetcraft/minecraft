# Proxy server

## Summary

Proxy server is a [Velocity proxy](https://velocitypowered.com/) which sits infront of a Minecraft Server. The proxy is configured with:

- [Geyser](https://github.com/GeyserMC/Geyser): allows bedrock clients to join
- [Floodgate](https://github.com/GeyserMC/Floodgate): allows bedrock clients to join without a Mojang account
- [ViaVersion](https://github.com/ViaVersion/ViaVersion): allows different Minecraft client version to join the server (in the case of our 1.15.2 server, it allows 1.15.x-1.17.x)
- [ViaBackwards](https://github.com/ViaVersion/ViaBackwards): allows older Minecraft clients to join the server (in the case of our 1.15.2 server, it allows 1.9.x-1.15.x)
- [ViaRewind](https://github.com/ViaVersion/ViaRewind): allows 1.8.x and 1.7.x clients to join the server

## Purpose

The purpose of this proxy server is provide cross compatibility with the Bedrock and Java edition of the game. It also aims to provide different version support. This way any client is able to join the server successfully.

## Build

The Velocity plugins were downloaded from the following locations and stored in the `libs` folder:

- https://ci.opencollab.dev/job/GeyserMC/job/Floodgate/job/master/lastSuccessfulBuild/artifact/velocity/target/floodgate-velocity.jar
- https://ci.opencollab.dev/job/GeyserMC/job/Geyser/job/master/lastSuccessfulBuild/artifact/bootstrap/velocity/target/Geyser-Velocity.jar
- https://github.com/ViaVersion/ViaVersion/releases/download/4.0.1/ViaVersion-4.0.1.jar
- https://github.com/ViaVersion/ViaBackwards/releases/download/4.0.1/ViaBackwards-4.0.1.jar
- https://ci.viaversion.com/view/ViaRewind/job/ViaRewind/104/artifact/all/target/ViaRewind-2.0.1.jar

## How to run

### Preferred

The suggested way we suggest you run the proxy server is by running the proxy with the OpenStreetCraft server. To do this enter the `bukkit-server` or `paper-server` directory and run:

```
docker-compose -f docker-compose.prod.yml up
```

This will start a proxy server + a Minecraft server connected to the public Openstreetcraft server. Now wait for the spawn to generate and once that is complete you are able to join on all your devices, with IP of your host. This is the optimal way to run it.

### Standalone

You can can build the proxy server with:

```
docker build -t proxy-server .
```

But we do not suggest you do this, by default the proxy server looks for a host called `java`. If you want to run this as a standalone server, you need to configure the DNS to point `java` to the address of your Minecraft Server. An alternative is to change the host of Minecraft Server in `config/velocity.toml`.

## Links / References

### Documentation

- [Velocity Documentation](https://velocitypowered.com/wiki)
- [Cubxity's Docker Minecraft Proxy](https://github.com/Cubxity/docker-minecraft-proxy)
- [Geyser Documentation](https://github.com/GeyserMC/Geyser/wiki)
- [Floodgate Documentation](https://github.com/GeyserMC/Floodgate)

### Discord Servers

Great places to get live help.

- [Velocity](https://discord.gg/8cB9Bgf)
- [Geyser / Floodgate](https://discord.gg/GeyserMC)
- [ViaVersion](https://discord.gg/viaversion)

